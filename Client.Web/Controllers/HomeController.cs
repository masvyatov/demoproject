using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Client.Web.Controllers
{
    public class HomeController : Controller
    {
        private IOptionsSnapshot<AppSettings> _settings;
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<HomeController> _logger;
        private readonly IHostingEnvironment _enviroment;

        public HomeController(IOptionsSnapshot<AppSettings> settings, IMemoryCache memoryCache, ILogger<HomeController> logger, IHostingEnvironment env)
        {
            _settings = settings;
            _memoryCache = memoryCache;
            _logger = logger;
            _enviroment = env;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Configuration = await GetConfigurationAsync();
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }

        public async Task<IActionResult> Configuration()
        {
            return Json(await GetConfigurationAsync());
        }

        [HttpGet("/images/product/{name:guid}.jpeg")]
        public async Task<IActionResult> Image(string name)
        {
            if (!Guid.TryParse(name, out var imageId))
            {
                return File("/images/product/default.jpeg", "image/jpeg");
            }

            var url = GetProductUrl($"/api/product/image/{name}");
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var bytes = await response.Content.ReadAsByteArrayAsync();
                        await WriteImage(name, bytes);
                        return File(bytes, response.Content.Headers.ContentType.MediaType);
                    }
                }
            }

            return File("/images/product/default.jpeg", "image/jpeg");
        }

        [HttpGet("/images/product/{name:guid}_sm.jpeg")]
        public async Task<IActionResult> ImageSmall(string name)
        {
            if (!Guid.TryParse(name, out var imageId))
            {
                return File("/images/product/default.jpeg", "image/jpeg");
            }

            var url = GetProductUrl($"/api/product/imagesmall/{name}");
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var bytes = await response.Content.ReadAsByteArrayAsync();
                        await WriteImage(name + "_sm", bytes);
                        return File(bytes, response.Content.Headers.ContentType.MediaType);
                    }
                }
            }

            return File("/images/product/default.jpeg", "image/jpeg");
        }

        private async Task WriteImage(string name, byte[] bytes)
        {
            var path = Path.Combine(_enviroment.WebRootPath, "images", "product", $"{name}.jpeg");
            if (System.IO.File.Exists(path))
                return;
            await System.IO.File.WriteAllBytesAsync(path, bytes);
        }

        private async Task<object> GetConfigurationAsync()
        {
            var value = _settings.Value;
            var settings = await GetSettingsInternalAsync();
            return new
            {
                value.LocationUrl,
                value.ProductUrl,
                settings.MaxPrice,
                settings.MaxPriceByMoney,
                settings.MinPrice,
                settings.StartTime,
                settings.EndTime,
                settings.DeliveryMinTime,
                settings.NonDeliveryDiscount,
                settings.WorkingToday,
                settings.MinNonDeliveryPrice,
            };
        }

        private async Task<ServerSetting> GetSettingsInternalAsync()
        {
            const string key = "SiteSettings";
            return await _memoryCache.GetOrCreateAsync(key, entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5);
                return GetDBSettingsAsync();
            });
        }

        private async Task<ServerSetting> GetDBSettingsAsync()
        {
            using (var client = new HttpClient())
            {
                var requestUri = GetProductUrl("/api/setting/Configuration");

                for (var i = 0; i < 10; i++)
                {
                    try
                    {
                        using (var response = await client.GetAsync(requestUri))
                        {
                            response.EnsureSuccessStatusCode();
                            var responseStr = await response.Content.ReadAsStringAsync();
                            _logger.LogError("url: {0}, response: {1}", requestUri.ToString(), responseStr);

                            return JsonConvert.DeserializeObject<ServerSetting>(responseStr);
                        }
                    }
                    catch
                    { }

                    await Task.Delay(TimeSpan.FromSeconds(1));
                }

                throw new Exception("Can't perform settings");
            }
        }

        private Uri GetProductUrl(string path)
        {
            var uriBuilrder = new UriBuilder(HttpContext.Request.Scheme, HttpContext.Request.Host.Host, HttpContext.Request.Host.Port ?? (HttpContext.Request.IsHttps ? 443 : 80));
            return new Uri(uriBuilrder.Uri, _settings.Value.ProductUrl + path);
        }

        class ServerSetting
        {
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public uint DeliveryMinTime { get; set; }
            public uint NonDeliveryDiscount { get; set; }
            public bool WorkingToday { get; set; }
            public uint MaxPrice { get; set; }
            public uint MinPrice { get; set; }
            public uint MinNonDeliveryPrice { get; set; }
            public uint MaxPriceByMoney { get; set; }
        }
    }
}
