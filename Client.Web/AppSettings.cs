﻿namespace Client.Web
{
    public class AppSettings
    {
        public string ProductUrl { get; set; }
        public string LocationUrl { get; set; }
    }
}
