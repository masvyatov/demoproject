import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';
import { StorageService } from './components/shared/services/storage.service';

export declare var ServerConfiguration: any;

export function getServerConfigurationFactory() {
    return ServerConfiguration;
}

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        BrowserModule,
        AppModuleShared
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl },
        { provide: 'ABSOLUTE_URL', useValue: '' },
        { provide: 'SERVER_CONFIGURATION', useFactory: getServerConfigurationFactory },
        StorageService
    ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
