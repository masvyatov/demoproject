import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { TextMaskModule } from './components/poc/mask/text-mask';
import { NouisliderModule } from 'ng2-nouislider';

import { PhoneFormatPipe } from './components/poc/pipe/phoneFormat';
import { PluralPipe } from './components/poc/pipe/plural';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { NavCardComponent } from './components/nav-card/nav-card.component';
import { HomeComponent } from './components/home/home.component';
import { ProductItemComponent } from './components/product-item/product-item.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductFreeComponent } from './components/product-free/product-free.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ButtonGroupComponent } from './components/poc/button-group/button-group.component';
import { FooterComponent } from './components/footer/footer.component';
import { LocationComponent } from './components/location/location.component';
import { CartComponent } from './components/card/cart.component';
import { LoginComponent } from './components/login/login.component';
import { TimeSelectorComponent } from './components/poc/time-selector/time-selector.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { OrderComponent } from './components/order/order.component';
import { ProfileComponent } from './components/profile/profile.component';
import { OfferComponent } from './components/pages/offer/offer.component';
import { DeliveryComponent } from './components/pages/delivery/delivery.component';
import { AboutComponent } from './components/pages/about/about.component';
import { ContactsComponent } from './components/pages/contacts/contacts.component';
import { ProductComponent } from './components/product/product.component';
import { MapComponent } from './components/poc/map/map.component';
import { AboutSharedComponent } from './components/poc/about-shared/about-shared.component';
import { OrderNumberComponent } from './components/order-number/order-number.component';
import { CommentsComponent } from './components/pages/comments/comments.component';
import { BonusComponent } from './components/pages/bonus/bonus.component';
import { MakeOrderComponent } from './components/make-order/make-order.component';
import { AdvantageComponent } from './components/advantage/advantage.component';
import { SliderComponent } from './components/poc/slider/slider/slider';
import { SlideComponent } from './components/poc/slider/slide/slide';

import { ConfigurationService } from './components/shared/services/configuration.service';
import { ProductService } from './components/product-item/product.service';
import { BasketService } from './components/shared/services/basket.service';
import { LocationService } from './components/shared/services/location.service';
import { LoginService } from './components/login/login.service';
import { SecurityService } from './components/shared/services/security.service';
import { YaService } from './components/shared/services/ya.service';
import { ProductRecomendationComponent } from './components/product-recomendation/product-recomendation.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        NavCardComponent,
        HomeComponent,
        ProductItemComponent,
        ProductListComponent,
        CarouselComponent,
        ButtonGroupComponent,
        FooterComponent,
        LocationComponent,
        CartComponent,
        LoginComponent,
        TimeSelectorComponent,
        CheckoutComponent,
        OrderComponent,
        ProfileComponent,
        ProductComponent,
        ProductFreeComponent,
        ProductRecomendationComponent,
        OrderNumberComponent,
        SliderComponent,
        SlideComponent,
        // static pages
        OfferComponent,
        DeliveryComponent,
        AboutComponent,
        ContactsComponent,
        MapComponent,
        AboutSharedComponent,
        CommentsComponent,
        BonusComponent,
        MakeOrderComponent,
        AdvantageComponent,
        // pipe
        PhoneFormatPipe,
        PluralPipe,
    ],
    imports: [
        NouisliderModule,
        TextMaskModule,
        CommonModule,
        ReactiveFormsModule,
        BrowserModule,
        HttpModule,
        JsonpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', component: HomeComponent },
            { path: 'cart', component: CartComponent },
            { path: 'login', component: LoginComponent },
            { path: 'checkout', component: CheckoutComponent },
            { path: 'order', component: OrderComponent },
            { path: 'profile', component: ProfileComponent },
            { path: 'prod/:name/:id', component: ProductComponent },

            // static pages
            { path: 'offer', component: OfferComponent },
            { path: 'delivery', component: DeliveryComponent },
            { path: 'about', component: AboutComponent },
            { path: 'contacts', component: ContactsComponent },
            { path: 'comments', component: CommentsComponent },
            { path: 'bonus', component: BonusComponent },

            { path: '**', redirectTo: '' }
        ]),
    ],
    providers: [
        ConfigurationService,
        ProductService,
        BasketService,
        LocationService,
        LoginService,
        SecurityService,
        YaService,
        {
            provide: LOCALE_ID,
            useValue: 'ru-RU'
        },
    ]
})
export class AppModuleShared {
}
