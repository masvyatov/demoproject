﻿import { Component, OnInit, ElementRef } from '@angular/core';
import { MapScriptUrl } from '../../shared/constants';

@Component({
    selector: 'map',
    template: '',
})
export class MapComponent implements OnInit {
    constructor(private elem: ElementRef) { }

    ngOnInit(): void {
        if (typeof document !== 'undefined') {
            const node = document.createElement('script');
            node.src = MapScriptUrl;
            node.type = 'text/javascript';
            node.async = true;
            node.charset = 'utf-8';
            this.elem.nativeElement.appendChild(node);
        }
    }
}
