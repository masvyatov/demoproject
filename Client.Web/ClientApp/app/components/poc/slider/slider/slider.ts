﻿import { Component, AfterContentInit, ElementRef, Input, HostListener, ContentChildren, QueryList } from "@angular/core";
import { SlideComponent } from "../slide/slide";

const defaultTransition = 'transform 0.5s ease 0s';

@Component({
    selector: 'slider',
    templateUrl: './slider.html',
    styleUrls: ['./slider.scss']
})
export class SliderComponent implements AfterContentInit {
    @Input()
    breakpoints: [number, number][] = [];

    @ContentChildren(SlideComponent) items?: QueryList<SlideComponent>;

    itemWidth = '0px';
    transform = 'translate3d(0px, 0px, 0px)';
    transition = defaultTransition;

    _page = 0;
    get page() {
        return this._page;
    }
    set page(val) {
        this._page = val;
        this.onTransformChanged();
    }
    pageCount: number[] = [];

    _width = 0;
    get width() {
        return this._width;
    }
    set width(val) {
        this._width = val;
        this.onChangeStyle();
    }

    // drag
    dragging = false;
    startTime = 0;
    dragStartX = 0;
    dragStartY = 0;
    minSwipeDistance = 8;
    dragOffset = 0;
    onEndHandle = (e: any) => this.onEnd(e);
    onDragHandle = (e: any) => this.onDrag(e);

    constructor(private el: ElementRef) { }

    ngAfterContentInit() {
        this.changeSize();
    }

    onChangeStyle() {
        const width = this.width;
        const count = this.getItemCountPerPage();
        this.itemWidth = `${width / count}px`;
        this.onPageCount();
        this.onTransformChanged();
    }

    onTransformChanged() {
        const count = this.getItemCountPerPage();
        const itemCount = this.items!.length;
        const translateTo = Math.max(Math.min(this.page, itemCount - count), 0);
        const translate = this.dragOffset + translateTo * parseInt(this.itemWidth);
        this.transform = `translate3d(${-translate}px, 0px, 0px)`;
        this.transition = this.dragging ? 'unset' : defaultTransition;
    }

    onPageCount() {
        const count = this.getItemCountPerPage();
        const itemCount = this.items!.length;
        const maxPageCount = Math.max(itemCount - count + 1, 0);
        this.pageCount = Array(maxPageCount).fill(0).map(i => i);
    }

    getItemCountPerPage(): number {
        const width = this.width;
        let count = 1;
        for (let point of this.breakpoints) {
            if (width >= point[0]) {
                count = point[1];
                break;
            }
        }
        return count;
    }

    @HostListener('touchstart', ['$event'])
    @HostListener('mousedown', ['$event'])
    onStart(e: any) {
        if (e.button === 2)
            return;
        const isTouch = e.type === 'touchstart';
        document.addEventListener(
            isTouch ? "touchend" : "mouseup",
            this.onEndHandle,
            true
        );
        document.addEventListener(
            isTouch ? "touchmove" : "mousemove",
            this.onDragHandle,
            true
        );
        this.startTime = e.timeStamp;
        this.dragging = true;
        this.dragStartX = isTouch ? e.touches[0].clientX : e.clientX;
        this.dragStartY = isTouch ? e.touches[0].clientY : e.clientY;
    }

    onEnd(e: any) {
        const isTouch = e.type === 'touchend';
        // compute the momemtum speed
        const eventPosX = isTouch ? e.changedTouches[0].clientX : e.clientX;
        const deltaX = this.dragStartX - eventPosX;

        if (Math.abs(deltaX) > this.minSwipeDistance) {
            const deltaPage = Math.sign(deltaX) * Math.ceil(Math.abs(deltaX) / parseInt(this.itemWidth));
            this.page += deltaPage;
            this.page = Math.max(Math.min(this.page, this.pageCount.length - 1), 0);
        }

        this.dragging = false;
        this.dragOffset = 0;
        this.onTransformChanged();
        // clear events listeners
        document.removeEventListener(
            isTouch ? "touchend" : "mouseup",
            this.onEndHandle,
            true
        );
        document.removeEventListener(
            isTouch ? "touchmove" : "mousemove",
            this.onDragHandle,
            true
        );
    }

    onDrag(e: any) {
        const isTouch = e.type === 'touchmove';
        const eventPosX = isTouch ? e.touches[0].clientX : e.clientX;
        const eventPosY = isTouch ? e.touches[0].clientY : e.clientY;
        const newOffsetX = this.dragStartX - eventPosX;
        const newOffsetY = this.dragStartY - eventPosY;
        // if it is a touch device, check if we are below the min swipe threshold
        // (if user scroll the page on the component)
        if (isTouch && Math.abs(newOffsetX) < Math.abs(newOffsetY)) {
            return;
        }
        e.stopImmediatePropagation();
        this.dragOffset = newOffsetX;
        this.onTransformChanged();
    }

    @HostListener('window:resize')
    changeSize() {
        this.width = this.el.nativeElement.clientWidth;
    }

    navigateTo(page: number): void {
        this.page = page;
    }
}