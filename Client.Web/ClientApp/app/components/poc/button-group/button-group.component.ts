﻿import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'button-group',
    templateUrl: './button-group.component.html',
    styleUrls: ['./button-group.component.scss']
})
export class ButtonGroupComponent {
    @Input() active: number = 0;
    @Input() values: number[] = [];
    @Input() prefix: string = '';

    @Output() onChange: EventEmitter<number> = new EventEmitter();

    constructor() {

    }

    changeActive(item: number): void {
        this.active = item;
        this.onChange.emit(item);
    }
}
