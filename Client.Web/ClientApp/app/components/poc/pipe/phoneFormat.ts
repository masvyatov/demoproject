﻿import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'phoneFormat'
})
export class PhoneFormatPipe implements PipeTransform {
    transform(phone: string): string {
        if (!phone || phone.length != 11)
            return phone;
        return phone.replace(/([7])([0-9]{3})([0-9]{3})([0-9]{4})/, '+$1 ($2) $3-$4');
    }
}