﻿import { Component, Input, OnInit, OnChanges, Output, EventEmitter, OnDestroy } from "@angular/core";

import { Observable } from "rxjs/Observable";

import { Subscription } from "rxjs";
import { ConfigurationService } from "../../shared/services/configuration.service";
import { IsoDate, BuildDate } from "../../shared/constants";

@Component({
    selector: 'time-selector',
    templateUrl: './time-selector.component.html',
    styleUrls: ['./time-selector.component.scss'],
})
export class TimeSelectorComponent implements OnInit, OnChanges, OnDestroy {
    @Input() date: string;
    @Output() onTimeChange: EventEmitter<string> = new EventEmitter();
    times: string[] = [];
    currentTime: string = '';
    private timerSubscription: Subscription | null = null;

    constructor(private configurationService: ConfigurationService) {
        this.date = IsoDate(new Date());
    }

    ngOnInit(): void {
        this.rebuildTimes();
        if (this.isToday() && typeof document !== 'undefined') {
            this.timerSubscription = Observable.timer(0, 1000 * 60)
                .subscribe(() => this.rebuildTimes());
        }
    }

    ngOnChanges(): void {
        this.rebuildTimes();
    }

    ngOnDestroy(): void {
        if (this.timerSubscription)
            this.timerSubscription.unsubscribe();
    }

    isToday(): boolean {
        return this.date === IsoDate(new Date());
    }

    setTime(time: string): void {
        this.currentTime = time;
        this.onTimeChange.next(time);
    }

    setNow(): void {
        let now = new Date();
        let time = this.getTime(now);
        this.setTime(time);
    }

    private rebuildTimes(): void {
        const interval = 15;
        const timeFrom = BuildDate(this.date, this.configurationService.serverSettings.StartTime);
        const timeTo = BuildDate(this.date, this.configurationService.serverSettings.EndTime);
        const times: string[] = [];
        const now = new Date();
        now.setSeconds(now.getSeconds() + this.configurationService.serverSettings.DeliveryMinTime);

        for (let time = timeFrom; time <= timeTo; time.setMinutes(time.getMinutes() + interval)) {
            if (now < time) {
                times.push(this.getTime(time));
            }
        }

        if (this.isToday() && times.length) {
            let time = new Date();
            time.setMinutes(time.getMinutes() + this.configurationService.serverSettings.DeliveryMinTime / 60);
            times.splice(0, 0, this.getTime(time));
        }

        this.times = times;
        if (this.times.length) {
            if (this.times.indexOf(this.currentTime) === -1) {
                this.setTime(this.times[0]);
            }
        }
        else {
            this.setTime('');
        }
    }

    private getTime(time: Date): string {
        let minutes = time.getMinutes() + '';
        let hours = time.getHours() + '';
        if (minutes.length === 1) {
            minutes = '0' + minutes;
        }
        if (hours.length === 1) {
            hours = '0' + hours;
        }

        return `${hours}:${minutes}`;
    }
}
