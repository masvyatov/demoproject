﻿import { Component, Inject, PLATFORM_ID, OnDestroy } from "@angular/core";
import { isPlatformBrowser } from "@angular/common";

import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";

import { OrderService } from "../order/order.service";

@Component({
    selector: 'order-number',
    templateUrl: 'order-number.component.html',
    styleUrls: ['order-number.component.scss'],
    providers: [OrderService],
})
export class OrderNumberComponent implements OnDestroy {
    orderNumber: number = 0;
    private timer: Subscription | null = null;

    constructor(private orderService: OrderService,
        @Inject(PLATFORM_ID) platformId: string) {
        if (isPlatformBrowser(platformId)) {
            this.timer = Observable.timer(0, 1000 * 60 * 5) // 5 min
                .subscribe(() => { this.updateOrderNumber(); });
        }
    }

    ngOnDestroy() {
        if (this.timer) {
            this.timer.unsubscribe();
        }
    }

    private updateOrderNumber(): void {
        this.orderService.getOrderNumber()
            .subscribe(number => this.orderNumber = number);
    }
}