﻿import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";

import { SecurityService } from "../shared/services/security.service";
import { PhoneFormatPipe } from "../poc/pipe/phoneFormat";
import { Profile } from "../shared/models/security.model";

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    providers: [PhoneFormatPipe]
})
export class ProfileComponent implements OnInit, OnDestroy {

    formGroup: FormGroup;
    profileSubscription: Subscription | null = null;
    bonus: number = 0;

    constructor(private securityService: SecurityService,
        private phoneFormatPipe: PhoneFormatPipe,
        private router: Router,
        private fb: FormBuilder) {
        this.formGroup = fb.group({
            name: ['', Validators.required],
            phone: ['', Validators.required],
        });
    }

    ngOnInit(): void {
        if (this.securityService.isAuthenficated) {
            if (!this.securityService.profile)
                return;

            this.formGroup.get('name')!.setValue(this.securityService.profile!.Name);
            let phone = this.phoneFormatPipe.transform(this.securityService.profile!.Phone);
            this.formGroup.get('phone')!.setValue(phone);
            this.bonus = this.securityService.profile!.Bonus;
        }
        this.profileSubscription = this.securityService.settingsLoaded$.subscribe(() => {
            if (!this.securityService.profile)
                return;

            this.formGroup.get('name')!.setValue(this.securityService.profile!.Name);
            let phone = this.phoneFormatPipe.transform(this.securityService.profile!.Phone);
            this.formGroup.get('phone')!.setValue(phone);
            this.bonus = this.securityService.profile!.Bonus;
        });
    }

    ngOnDestroy(): void {
        if (this.profileSubscription)
            this.profileSubscription.unsubscribe();
    }

    save(): void {
        let profile: Profile = {
            Name: this.formGroup.get('name')!.value,
            Phone: this.formGroup.get('phone')!.value,
            Bonus: 0,
        };
        this.securityService.save(profile)
            .subscribe(resp => {
                let form = this.formGroup.get('name');
                form!.reset(resp.Name);
            });
    }

    logout(): void {
        this.securityService.logout()
            .subscribe(() => {
                this.router.navigate(['/']);
            });
    }
}