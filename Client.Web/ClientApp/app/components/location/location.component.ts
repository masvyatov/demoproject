﻿import { Component, OnInit, OnDestroy, EventEmitter, Output } from "@angular/core";
import { FormControl } from "@angular/forms";

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs";

import { LocationService } from "../shared/services/location.service";
import { IStreetModel, IBuildingModel, DeliveryLocation } from "../shared/models/location.model";

@Component({
    selector: 'location',
    templateUrl: './location.component.html',
    styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit, OnDestroy {
    @Output() onClose: EventEmitter<void> = new EventEmitter();

    streetControl = new FormControl();
    streetCtrlSub: Subscription | null = null;
    streetChangeCtrlSub: Subscription | null = null;
    possibleStreets: IStreetModel[] = [];
    currentStreet: IStreetModel | null = null;

    buildingControl = new FormControl();
    buildingCtrlSub: Subscription | null = null;
    buildingChangeCtrlSub: Subscription | null = null;
    possibleBuildings: IBuildingModel[] = [];
    currentBuilding: IBuildingModel | null = null;

    constructor(private locationService: LocationService) {
    }

    ngOnInit() {
        this.streetCtrlSub = this.streetControl.valueChanges
            .debounceTime(500)
            .subscribe(newValue => {
                this.streetNameChanged(newValue);
            });
        this.streetChangeCtrlSub = this.streetControl.valueChanges
            .subscribe(() => {
                this.currentStreet = null;
                this.currentBuilding = null;
                this.possibleBuildings = [];
                this.buildingControl.setValue('', { emitEvent: false });
            });

        this.buildingCtrlSub = this.buildingControl.valueChanges
            .debounceTime(500)
            .subscribe(newValue => {
                if (this.currentStreet != null)
                    this.buildingNameChanged(newValue, this.currentStreet.StreetId);
            });
        this.streetChangeCtrlSub = this.buildingControl.valueChanges
            .subscribe(() => {
                this.currentBuilding = null;
            });
    }

    ngOnDestroy() {
        if (this.streetCtrlSub != null)
            this.streetCtrlSub.unsubscribe();
        if (this.streetChangeCtrlSub != null)
            this.streetChangeCtrlSub.unsubscribe();
        if (this.buildingCtrlSub != null)
            this.buildingCtrlSub.unsubscribe();
        if (this.buildingChangeCtrlSub != null)
            this.buildingChangeCtrlSub.unsubscribe();
    }

    private streetNameChanged(streetName: string): void {
        this.locationService.getStreets(streetName)
            .subscribe(streets => {
                this.possibleStreets = streets;
            });
    }

    setStreet(street: IStreetModel): void {
        this.currentStreet = street;
        this.streetControl.setValue([street.CityName, street.StreetName].join(', '), { emitEvent: false });
        this.possibleStreets = [];
    }

    setBuilding(building: IBuildingModel): void {
        this.currentBuilding = building;
        this.buildingControl.setValue(building.Name, { emitEvent: false });
        this.possibleBuildings = [];
    }

    private buildingNameChanged(buildingName: string, streetId: string): void {
        this.locationService.getBuildings(buildingName, streetId)
            .subscribe(buildings => {
                this.possibleBuildings = buildings;
            });
    }

    acceptAddress(): void {
        if (this.currentBuilding !== null && this.currentStreet !== null &&
            this.currentBuilding.Avalable && this.currentStreet.Avalable) {
            const delivery = new DeliveryLocation(this.currentStreet.CityName,
                this.currentStreet.StreetName,
                this.currentBuilding.Name,
                this.currentBuilding.Id);
            this.locationService.setDeliveryLocation(delivery);
        }
        this.close();
    }

    close() {
        this.locationService.locationChanged$.emit();
        this.onClose.emit();
    }
}
