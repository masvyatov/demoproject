﻿import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";

import { SecurityService } from "../shared/services/security.service";
import { BasketService } from "../shared/services/basket.service";
import { ConfigurationService } from "../shared/services/configuration.service";
import { YaService } from "../shared/services/ya.service";
import { BasketItem } from "../shared/models/basket.model";
import { Souces, Drinks, Flatwares } from "../shared/constants";
import { ProductService } from "../product-item/product.service";

@Component({
    selector: 'cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {
    basketSubscription: Subscription | null = null;
    productUrls: { [key: string]: string } = {};
    cartItems: BasketItem[] = [];
    amount: number = 0;
    maxPrice: number = 10000000;

    flatwareGroupName = Flatwares;
    souceGroupName = Souces;
    drinkGroupName = Drinks;

    constructor(public basketService: BasketService,
        private securityService: SecurityService,
        private router: Router,
        private configurationService: ConfigurationService,
        private yaService: YaService,
        private productService: ProductService
    ) { }

    ngOnInit(): void {
        this.onBasketChanged();
        this.basketSubscription = this.basketService.basket.changed$.subscribe(() => {
            this.onBasketChanged();
        });

        this.maxPrice = this.configurationService.serverSettings.MaxPrice;
        this.yaService.reachGoal('enter_to_card');
    }

    ngOnDestroy(): void {
        if (this.basketSubscription)
            this.basketSubscription.unsubscribe();
    }

    removeFromCard(item: BasketItem): void {
        const basketItem = new BasketItem(item.ProductId,
            item.Name,
            item.Count - 1,
            item.Price
        );
        this.basketService.update(basketItem)
            .subscribe();
    }

    addToCard(item: BasketItem): void {
        if (!item.canAddProduct())
            return;

        const basketItem = new BasketItem(item.ProductId,
            item.Name,
            item.Count + 1,
            item.Price
        );
        this.basketService.update(basketItem)
            .subscribe();
    }

    removeInCart(item: BasketItem): void {
        const basketItem = new BasketItem(item.ProductId,
            item.Name,
            0,
            item.Price
        );
        this.basketService.update(basketItem)
            .subscribe();
    }

    isDisabled(): boolean {
        return this.amount <= 0 || this.amount > this.maxPrice || !this.isForSale() || !this.isValidCount();
    }

    isValidCount(): boolean {
        return this.cartItems.filter(m => m.isValidCount === false).length === 0;
    }

    isForSale(): boolean {
        return this.cartItems.filter(m => m.IsForSale === false).length === 0;
    }

    accept(): void {
        if (!this.securityService.isAuthenficated) {
            this.router.navigate(['/login'], {
                queryParams: { returnUrl: '/checkout' }
            });
            return;
        }
        this.router.navigate(['/checkout']);
    }

    onBasketChanged(): void {
        this.cartItems = this.basketService.basket.Items.slice();
        this.amount = this.basketService.basket.Amount;
        this.buildProductImageUrls();
    }

    getImage(item: BasketItem): string {
        let image = this.productUrls[item.ProductId];
        if (image == null)
            return ProductService.getSmallImageUrl([]);
        return image;
    }

    buildProductImageUrls() {
        this.productService.getProductList()
            .subscribe(products => {
                const imageUrls: { [key: string]: string } = {};
                products.forEach(val => {
                    imageUrls[val.ProductId] = ProductService.getSmallImageUrl(val.ImageIds);
                });
                this.productUrls = imageUrls;
            });
    }
}
