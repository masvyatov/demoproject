﻿import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from "@angular/core";

import { Subscription } from "rxjs/Subscription";

import { BasketService } from "../shared/services/basket.service";
import { ProductService } from "../product-item/product.service";

import { Product } from "../product-item/product.data";
import { BasketFreeItem, BasketItem } from "../shared/models/basket.model";

@Component({
    selector: 'product-free',
    templateUrl: './product-free.component.html',
    styleUrls: ['./product-free.component.scss']
})
export class ProductFreeComponent implements OnInit, OnChanges, OnDestroy {
    products: Product[] = [];
    basketSubscription: Subscription | null = null;
    @Input() groupName: string | null = null;
    free: BasketFreeItem | null = null;

    constructor(private basketService: BasketService,
        private productService: ProductService) { }

    ngOnInit(): void {
        this.basketSubscription = this.basketService.basket.changed$.subscribe(() => {
            this.onBasketChanged();
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.onBasketChanged();

        this.productService.getProductList()
            .subscribe(products => {
                this.products = products.filter(m => m.GroupName === this.groupName && m.CanFree)
                    .map(product => ProductService.toProductList(product))
                    .sort((a, b) => {
                        if (a.Name < b.Name)
                            return -1;
                        if (a.Name > b.Name)
                            return 1;
                        return 0;
                    });

                this.onBasketChanged();
            });
    }

    ngOnDestroy(): void {
        if (this.basketSubscription)
            this.basketSubscription.unsubscribe();
    }

    private onBasketChanged(): void {
        
    }

    count(product: Product): number {
        let products = this.basketService.basket.Items.filter(m => m.ProductId === product.ProductId);
        if (products.length) {
            return products[0].Count;
        }

        return 0;
    }

    addToCard(product: Product): void {
        let count = this.count(product);
        if (count >= 99)
            return;

        if (product && product.SaleCount != null && product.SaleCount <= count)
            return;

        count++;
        this.updateBasket(product, count);
    }

    removeFromCard(product: Product): void {
        let count = this.count(product);
        count--;
        this.updateBasket(product, count);
    }

    private updateBasket(product: Product, count: number): void {
        const basketItem: BasketItem = new BasketItem(product.ProductId,
            product.Name,
            count,
            product.Price);

        this.basketService.update(basketItem)
            .subscribe();
    }

    getPrice = (product: Product): number => ProductService.getProductPrice(product) || 0;
}
