﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductGroupList } from '../product-item/product.data';
import { ProductService } from '../product-item/product.service';

@Component({
    selector: 'product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
    productGroups: ProductGroupList[] = [];

    constructor(private productService: ProductService,
        private activatedRoute: ActivatedRoute,
    ) {
    }

    ngOnInit(): void {
        this.productService.getProductGroupList()
            .subscribe(productGroupLists => {
                this.productGroups = productGroupLists;

                setTimeout(() => {
                    this.tryNavigation();
                }, 0);
            });
    }

    private tryNavigation(): void {
        this.activatedRoute.fragment.subscribe(val => {
            if (!val)
                return;

            const element = document.querySelector('#' + val);
            if (element) {
                element.scrollIntoView({ behavior: 'smooth', block: 'start' });
            }
        });
    }
}