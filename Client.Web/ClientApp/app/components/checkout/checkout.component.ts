﻿import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { Observable } from "rxjs/Observable";

import { SecurityService } from "../shared/services/security.service";
import { BasketService } from "../shared/services/basket.service";
import { ConfigurationService } from "../shared/services/configuration.service";
import { YaService } from "../shared/services/ya.service";
import { BasketItem } from "../shared/models/basket.model";
import { PhoneFormatPipe } from "../poc/pipe/phoneFormat";
import { IStreetModel, IBuildingModel } from "../shared/models/location.model";
import { LocationService } from "../shared/services/location.service";
import { OrderService } from "../order/order.service";
import { CreateLeadDeliveryModel, OrderProductItem, CreateOrderModel, IOrderDelivery } from "../order/order.model";
import { IsoDate, BuildDate } from "../shared/constants";

type DateType = 'today' | 'tomorrow';
const PaymentType = {
    ByCard: 1,
    ByMoney: 2
};

const DeliveryType = {
    Delivery: 1,
    None: 2
};

@Component({
    selector: 'checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss'],
    providers: [PhoneFormatPipe, OrderService]
})
export class CheckoutComponent implements OnInit, OnDestroy {
    loading = false;
    formDirty = false;

    basketSubscription: Subscription | null = null;
    cartItems: BasketItem[] = [];
    amount: number = 0;
    maxPrice: number = 1000000;
    maxPriceByMoney: number = 1000000;

    // step 2
    profileSubscription: Subscription | null = null;
    dateType: DateType = 'today';
    date: string = IsoDate(new Date());
    time: string = '';
    formGroup: FormGroup;
    checkSum: boolean = false;

    streetCtrlSub: Subscription | null = null;
    streetChangeCtrlSub: Subscription | null = null;
    possibleStreets: IStreetModel[] = [];
    currentStreet: IStreetModel | null = null;

    buildingCtrlSub: Subscription | null = null;
    buildingChangeCtrlSub: Subscription | null = null;
    possibleBuildings: IBuildingModel[] = [];
    currentBuilding: IBuildingModel | null = null;

    // deliveries
    deliveries: IOrderDelivery[] = [];
    currentDelivery: IOrderDelivery | null = null;
    deliveryType: number = DeliveryType.Delivery;
    DeliveryType = DeliveryType;

    // promocode
    promocode: string = '';
    discount: number = 0;
    promoApplyed: boolean | null = null;

    // bonus
    maxBonus: number = 0;
    bonusApplyed: boolean = false;

    // references
    @ViewChild("name") nameRef: ElementRef | null = null;
    @ViewChild("street") streetRef: ElementRef | null = null;
    @ViewChild("building") buildingRef: ElementRef | null = null;
    @ViewChild("delivery") deliveryRef: ElementRef | null = null;

    constructor(private basketService: BasketService,
        private orderService: OrderService,
        private securityService: SecurityService,
        private locationService: LocationService,
        private router: Router,
        private fb: FormBuilder,
        private phoneFormatPipe: PhoneFormatPipe,
        private configurationService: ConfigurationService,
        private yaService: YaService
    ) {
        this.formGroup = this.fb.group({
            amount: [0, Validators.compose([this.minPriceValidator(), this.maxPriceValidator()])],
            name: ['', Validators.required],
            phone: ['', Validators.required],
            time: ['', Validators.required],

            street: ['', Validators.compose([Validators.required, this.streetValidator()])],
            house: ['', Validators.compose([Validators.required, this.houseValidator()])],
            buiding_id: ['', Validators.required],
            order_delivery_id: [''],
            apartment: [''],
            entance: [''],
            door_code: [''],
            floor: [''],
            delivery_name: ['', Validators.required],

            bonus: [0],

            payment_type: [PaymentType.ByCard, Validators.compose([Validators.required, this.maxPriceByMoneyValidator()])],
            payment_sum: [''],
            comment: ['']
        });

        this.formGroup.get('phone')!.disable();

        const streetControl = this.formGroup.get('street');
        const buildingControl = this.formGroup.get('house');

        this.streetCtrlSub = streetControl!.valueChanges
            .debounceTime(500)
            .subscribe(newValue => {
                this.streetNameChanged(newValue);
            });
        this.streetChangeCtrlSub = streetControl!.valueChanges
            .subscribe(() => {
                this.currentStreet = null;
                this.currentBuilding = null;
                this.possibleBuildings = [];
                buildingControl!.setValue('', { emitEvent: false });
                this.currentStreetChanged();
            });

        this.buildingCtrlSub = buildingControl!.valueChanges
            .debounceTime(500)
            .subscribe(newValue => {
                if (this.currentStreet != null)
                    this.buildingNameChanged(newValue, this.currentStreet.StreetId);
            });
        this.streetChangeCtrlSub = buildingControl!.valueChanges
            .subscribe(() => {
                this.setBuilding(null);
            });

        this.setDeliveryType(DeliveryType.Delivery);
    }

    ngOnInit(): void {
        this.cartItems = this.basketService.basket.Items;
        this.amount = this.basketService.basket.Amount;
        this.formGroup.get('amount')!.setValue(this.getAmount());

        this.basketSubscription = this.basketService.basket.changed$.subscribe(() => {
            this.cartItems = this.basketService.basket.Items;
            this.amount = this.basketService.basket.Amount;
            this.formGroup.get('amount')!.setValue(this.getAmount());
            this.formGroup.get('bonus')!.setValue(this.getMaxBonus());
        });

        if (this.securityService.isAuthenficated) {
            const profile = this.securityService.profile;
            this.formGroup.get('name')!.setValue(profile!.Name);
            let phone = this.phoneFormatPipe.transform(profile!.Phone);
            this.formGroup.get('phone')!.setValue(phone);
            this.maxBonus = profile!.Bonus;
            this.formGroup.get('bonus')!.setValue(this.getMaxBonus());
            if (this.maxBonus == 0) {
                this.formGroup.get('bonus')!.disable();
            }
        }
        this.profileSubscription = this.securityService.settingsLoaded$.subscribe(() => {
            if (this.securityService.profile) {
                const profile = this.securityService.profile;
                this.formGroup.get('name')!.setValue(profile!.Name);
                let phone = this.phoneFormatPipe.transform(profile!.Phone);
                this.formGroup.get('phone')!.setValue(phone);
                this.maxBonus = profile!.Bonus;
                this.formGroup.get('bonus')!.setValue(this.getMaxBonus());
                if (this.maxBonus == 0) {
                    this.formGroup.get('bonus')!.disable();
                }
            }
        });

        if (this.canDeliveryToday()) {
            this.setDate('today');
        }
        else {
            this.setDate('tomorrow');
        }

        this.maxPrice = this.configurationService.serverSettings.MaxPrice;
        this.maxPriceByMoney = this.configurationService.serverSettings.MaxPriceByMoney;

        this.orderService.getOrderDeliveries()
            .subscribe(res => {
                this.deliveries = res || [];
            });

        this.yaService.reachGoal('checkout');
    }

    ngOnDestroy(): void {
        if (this.basketSubscription)
            this.basketSubscription.unsubscribe();
        if (this.profileSubscription)
            this.profileSubscription.unsubscribe();

        if (this.streetCtrlSub != null)
            this.streetCtrlSub.unsubscribe();
        if (this.streetChangeCtrlSub != null)
            this.streetChangeCtrlSub.unsubscribe();
        if (this.buildingCtrlSub != null)
            this.buildingCtrlSub.unsubscribe();
        if (this.buildingChangeCtrlSub != null)
            this.buildingChangeCtrlSub.unsubscribe();
    }

    canDeliveryToday(): boolean {
        if (!this.configurationService.serverSettings.WorkingToday)
            return false;

        let now = new Date();
        let endTime = BuildDate(IsoDate(now), this.configurationService.serverSettings.EndTime);
        now.setSeconds(now.getSeconds() + this.configurationService.serverSettings.DeliveryMinTime);
        return now < endTime;
    }

    getDateClass(date: DateType): string[] {
        const active = date == this.dateType;
        return active ? ['active', 'btn-date', 'btn'] : ['btn-date', 'btn'];
    }

    setDate(date: DateType): void {
        this.dateType = date;
        const now = new Date();
        switch (date) {
            case 'today':
                this.date = IsoDate(now);
                break;
            case 'tomorrow':
                now.setHours(now.getHours() + 24);
                this.date = IsoDate(now);
                break;
        }
        this.onTimeStampChanged();
    }

    setName(name: string): void {
        this.formGroup.get('delivery_name')!.setValue(name);
    }

    onTimeChange(time: string): void {
        this.time = time;
        this.onTimeStampChanged();
    }

    onTimeStampChanged(): void {
        let timeControl = this.formGroup.get('time');
        if (this.date && this.time) {
            const timeTs = BuildDate(this.date, this.time).getTime() / 1000;
            timeControl!.setValue(timeTs);
        }
        else {
            timeControl!.setValue('');
        }
    }

    get paymentType(): number {
        const value = this.formGroup.get('payment_type')!.value;
        return parseInt(value);
    }

    setPaymentType(type: number): void {
        this.formGroup.get('payment_type')!.setValue(type);
    }

    setCheckAllSum(): void {
        this.checkSum = !this.checkSum;
        let sum = this.formGroup.get('payment_sum');
        if (this.checkSum) {
            sum!.disable();
            sum!.setValue(this.getAmount());
        }
        else {
            sum!.enable();
        }
    }

    getStep2Errors(): { [key: string]: any } {
        const keys = Object.keys(this.formGroup.controls);
        let obj: any = {};
        for (let key of keys) {
            const contrl = this.formGroup.get(key);
            if (contrl != null && contrl.errors != null) {
                obj[key] = contrl!.errors;
            }
        }
        return obj;
    }

    // create order
    accept(): void {
        if (this.formGroup.valid) {
            this.createOrder();
            return;
        }
        else {
            let errors = this.getStep2Errors();
            let ref: ElementRef | null = null;
            for (let error in errors) {
                switch (error) {
                    case 'name':
                        ref = this.nameRef;
                        break;
                    case 'street':
                        ref = this.streetRef;
                        break;
                    case 'buiding_id':
                    case 'house':
                        ref = this.buildingRef;
                        break;
                    case 'delivery_name':
                        ref = this.deliveryRef;
                        break;
                    default:
                        console.log('navigateTo', error);
                        break;
                }
                break;
            }
            if (ref) {
                ref.nativeElement.focus();
            }
        }

        this.formDirty = true;
    }

    createOrder(): void {
        const basket = this.basketService.basket;
        const form = this.formGroup;

        let leadDelivery: CreateLeadDeliveryModel = {
            Apartment: form.get('apartment')!.value,
            BuildingId: form.get('buiding_id')!.value,
            DoorCode: form.get('door_code')!.value,
            Entance: form.get('entance')!.value,
            Floor: form.get('floor')!.value,
            Name: form.get('delivery_name')!.value,
            Street: form.get('street')!.value,
            House: form.get('house')!.value,
        };

        let items: OrderProductItem[] = basket.Items.map(m => {
            let item: OrderProductItem = {
                Amount: m.Amount,
                Count: m.Count,
                ProductId: m.ProductId
            };
            return item;
        });

        let model: CreateOrderModel = {
            LeadName: form.get('name')!.value,
            Amount: this.amount,
            Bonus: this.bonusApplyed ? form.get('bonus')!.value : 0,
            Comment: form.get('comment')!.value,
            OrderDeliveryId: form.get('order_delivery_id')!.value || null,
            DeliveryTime: form.get('time')!.value,
            PaymentType: form.get('payment_type')!.value,
            PaymentSum: form.get('payment_sum')!.value || null,
            Discount: this.getDiscount(),
            PromoCode: this.promocode,
            DeliveryType: this.deliveryType,
            CreateLeadDelivery: this.deliveryType == DeliveryType.Delivery ? leadDelivery : null,
            Items: items
        };

        this.loading = true;
        this.orderService.createOrder(model)
            .catch(error => {
                this.loading = false;
                return Observable.throw(error);
            })
            .subscribe(res => {
                this.loading = false;
                if (res && res.Success) {
                    this.basketService.clear();
                    this.securityService.load();
                    this.yaService.reachGoal('order_success');

                    if (res.RedirectUrl) {
                        window.location.href = res.RedirectUrl;
                    }
                    else {
                        this.router.navigate(['/order'], {
                            queryParams: {
                                OrderId: res.OrderId
                            }
                        });
                    }
                }
            });
    }

    // location
    private streetNameChanged(streetName: string): void {
        if (!streetName) {
            this.possibleStreets = [];
            return;
        }

        this.locationService.getStreets(streetName)
            .subscribe(streets => {
                this.possibleStreets = streets || [];
            });
    }

    private buildingNameChanged(buildingName: string, streetId: string): void {
        if (!buildingName) {
            this.possibleBuildings = [];
            return;
        }

        this.locationService.getBuildings(buildingName, streetId)
            .subscribe(buildings => {
                this.possibleBuildings = buildings || [];
            });
    }

    setStreet(street: IStreetModel): void {
        const streetControl = this.formGroup.get('street');

        this.currentStreet = street;
        streetControl!.setValue([street.CityName, street.StreetName].join(', '), { emitEvent: false });
        this.possibleStreets = [];
        this.currentStreetChanged();
    }

    setBuilding(building: IBuildingModel | null): void {
        const buildingControl = this.formGroup.get('house');
        const buildingIdControl = this.formGroup.get('buiding_id');

        this.currentBuilding = building;
        this.possibleBuildings = [];

        if (building != null) {
            buildingControl!.setValue(building.Name, { emitEvent: false });
            if (building.Avalable) {
                buildingIdControl!.setValue(building.Id);
            }
        }
        else {
            buildingIdControl!.setValue('');
        }
    }

    autocompleteKeyUp(event: any): void {
        switch (event.keyCode) {
            // ArrowDown
            case 40: {
                let el = event.srcElement;
                while ((el = el!.parentElement) && !el.classList.contains('form-group'));
                let items = el!.querySelectorAll("li");

                if (!items.length)
                    break;

                let shouldNext = false;
                let activeElement = document.activeElement;
                for (let li of items) {
                    if (shouldNext) {
                        li.focus();
                        return;
                    }
                    if (activeElement == li) {
                        shouldNext = true;
                    }
                }
                if (!shouldNext) {
                    items[0].focus();
                }
                break;
            }
            // ArrowUp
            case 38: {
                let el = event.srcElement;
                while ((el = el!.parentElement) && !el.classList.contains('form-group'));
                let items = el!.querySelectorAll("li");

                if (!items.length)
                    break;

                let previos: any = null;
                let activeElement = document.activeElement;
                for (let li of items) {
                    if (activeElement == li) {
                        if (previos) {
                            previos.focus();
                        }
                        return;
                    }

                    previos = li;
                }

                items[0].focus();
                break;
            }
            // escape
            case 27: {
                let el = event.srcElement;
                while ((el = el!.parentElement) && !el.classList.contains('form-group'));
                let items = el!.querySelectorAll("input");
                if (items.length) {
                    items[0].focus();
                }

                break;
            }
        }
    }

    onHouseBlur(event: any): void {
        if (event && event.relatedTarget && event.relatedTarget.nodeName === "LI")
            return;

        if (this.possibleBuildings) {
            const houseName = this.formGroup.get('house')!.value || "";
            let buildings = this.possibleBuildings.filter(m => m.Name.toLowerCase() === houseName);
            if (buildings.length) {
                this.setBuilding(buildings[0]);
            }
        }
    }

    currentStreetChanged(): void {
        let houseControl = this.formGroup.get('house')!;
        let disabled = this.currentStreet == null || !this.currentStreet.Avalable;
        if (disabled) {
            houseControl.disable();
        }
        else {
            houseControl.enable();
        }
    }

    // validators
    get minPrice(): number {
        if (this.deliveryType === DeliveryType.None)
            return this.configurationService.serverSettings.MinNonDeliveryPrice;

        return this.configurationService.serverSettings.MinPrice;
    }

    minPriceValidator(): ValidatorFn {
        let that = this;
        return (c: AbstractControl) => {
            if (that.minPrice > that.getAmount()) {
                return {
                    required: true
                };
            }
            return null;
        };
    }

    maxPriceValidator(): ValidatorFn {
        let that = this;
        return (c: AbstractControl) => {
            if (that.maxPrice < that.getAmount()) {
                return {
                    required: true
                };
            }
            return null;
        };
    }

    maxPriceByMoneyValidator(): ValidatorFn {
        let that = this;
        return (c: AbstractControl) => {
            if (!that.formGroup) {
                return null;
            }

            let paymentType = that.formGroup.get('payment_type')!.value;
            if (that.maxPriceByMoney < that.getAmount() && paymentType == PaymentType.ByMoney) {
                return {
                    required: true
                };
            }
            return null;
        };
    }

    streetValidator(): ValidatorFn {
        let that = this;
        return (c: AbstractControl) => {
            if (that.currentDelivery != null)
                return null;
            if (that.currentStreet == null || !that.currentStreet.Avalable) {
                return {
                    required: true
                };
            }
            return null;
        };
    }

    houseValidator(): ValidatorFn {
        let that = this;
        return (c: AbstractControl) => {
            if (that.currentDelivery != null)
                return null;
            if (that.currentBuilding == null || !that.currentBuilding.Avalable) {
                return {
                    required: true
                };
            }
            return null;
        };
    }

    // delivery
    setDelivery(delivery: IOrderDelivery | null): void {
        this.currentDelivery = delivery;
        if (delivery == null) {
            this.formGroup.get('street')!.setValue('', { emitEvent: false });
            this.formGroup.get('house')!.setValue('', { emitEvent: false });
            this.formGroup.get('delivery_name')!.setValue('');
            this.formGroup.get('order_delivery_id')!.setValue('');
            this.formGroup.get('buiding_id')!.setValue(null);
        }
        else {
            this.formGroup.get('street')!.setValue(delivery.StreetName);
            this.formGroup.get('house')!.setValue(delivery.BuildingName);
            this.formGroup.get('delivery_name')!.setValue(delivery.Name);
            this.formGroup.get('order_delivery_id')!.setValue(delivery.LeadDeliveryId);
            this.formGroup.get('buiding_id')!.setValue(delivery.BuildingId);
        }
    }

    getAmountWithBonus(): number {
        let amount = this.getAmount();
        if (this.bonusApplyed) {
            amount -= this.formGroup.get('bonus')!.value;
        }

        if (amount < 0) {
            this.onBonusChanged();
        }

        return amount;
    }

    getAmount(): number {
        return this.calcPrice(this.amount);
    }

    private calcPrice(price: number): number {
        return price * (1 - this.getDiscount() / 100);
    }

    private getDiscount(): number {
        let discount = this.deliveryType == DeliveryType.None ? this.configurationService.serverSettings.NonDeliveryDiscount : 0;
        return Math.max(discount, this.discount);
    }

    setDeliveryType(type: number): void {
        this.deliveryType = type;

        const requireControl = [
            this.formGroup.get('street'),
            this.formGroup.get('house'),
            this.formGroup.get('buiding_id'),
            this.formGroup.get('delivery_name'),
        ];
        const noneRequiredControl = [
            this.formGroup.get('order_delivery_id'),
            this.formGroup.get('apartment'),
            this.formGroup.get('entance'),
            this.formGroup.get('door_code'),
            this.formGroup.get('floor'),
        ];

        if (type == DeliveryType.None) {
            for (let control of requireControl) {
                control!.setValidators(null);
            }
        }
        else {
            for (let control of requireControl) {
                control!.setValidators(Validators.required);
            }
            this.formGroup.get('street')!.setValidators(Validators.compose([Validators.required, this.streetValidator()]));
            this.formGroup.get('house')!.setValidators(Validators.compose([Validators.required, this.houseValidator()]))
        }

        for (let control of requireControl) {
            control!.setValue('');
        }
        for (let control of noneRequiredControl) {
            control!.setValue('');
        }

        this.formGroup.get('amount')!.updateValueAndValidity();
    }

    // bonus
    onBonusChanged(): void {
        const bonusControl = this.formGroup.get('bonus');
        let bonus: number = bonusControl!.value;
        bonus = Math.max(0, bonus);
        bonus = Math.min(this.maxBonus, bonus);
        bonus = Math.min(this.getAmount(), bonus);
        bonusControl!.setValue(bonus);

        if (this.maxBonus == 0) {
            bonusControl!.disable();
        }
        else if (!this.bonusApplyed) {
            bonusControl!.enable();
        }
    }

    applyBonus(): void {
        const bonusControl = this.formGroup.get('bonus');
        if (!this.bonusApplyed) {
            this.onBonusChanged();
            bonusControl!.disable();
        }
        else {
            bonusControl!.enable();
        }

        this.bonusApplyed = !this.bonusApplyed;
    }

    getMaxBonus(): number {
        return Math.min(this.getAmount(), this.maxBonus);
    }

    // promocode
    applyPromo(): void {
        if (!this.promoApplyed) {
            this.orderService.applyPromo(this.promocode)
                .subscribe(discount => {
                    if (discount != null) {
                        this.discount = discount;
                        this.promoApplyed = true;
                    }
                    else {
                        this.promoApplyed = false;
                    }
                });
        }
        else {
            this.discount = 0;
            this.promocode = '';
            this.promoApplyed = null;
        }
    }
}
