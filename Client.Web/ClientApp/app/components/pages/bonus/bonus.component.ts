﻿import { Component, OnInit, OnDestroy } from "@angular/core";

import { SecurityService } from "../../shared/services/security.service";
import { Subscription } from "rxjs";

@Component({
    templateUrl: './bonus.component.html',
    styleUrls: ['./bonus.component.scss']
})
export class BonusComponent implements OnInit, OnDestroy {
    bonus: number = 0;
    displayBonus: boolean = false;
    securitySubscription: Subscription;

    constructor(private securityService: SecurityService) {
        this.onBonusChanged();
        this.securitySubscription = this.securityService.settingsLoaded$.subscribe(() => this.onBonusChanged())
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {
        if (this.securitySubscription != null) {
            this.securitySubscription.unsubscribe();
        }
    }

    private onBonusChanged() {
        if (this.securityService.isAuthenficated) {
            this.bonus = this.securityService.profile!.Bonus;
        }
        this.displayBonus = this.securityService.isAuthenficated;
    }
}