﻿import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptionsArgs } from "@angular/http";

import { Observable } from "rxjs/Observable";
import { ConfigurationService } from "../../shared/services/configuration.service";

@Injectable()
export class CommentsService {

    constructor(private configurationService: ConfigurationService,
        private http: Http) { }

    get(): Observable<CommentItem[]> {
        const url = this.getProductUrl() + '/api/comment/GetComments';
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, {}, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    createComment(model: CreateComment): Observable<boolean> {
        const url = this.getProductUrl() + '/api/comment/AddComment';
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    private getProductUrl(): string {
        return this.configurationService.serverSettings.ProductUrl;
    }
}

export interface CommentItem {
    Name: string;
    CreatedOn: number;
    Text: string;
    RepliedOn: number | null;
    Reply: string;
}

export interface CreateComment {
    Name: string;
    CreatedOn: number;
    Text: string;
}
