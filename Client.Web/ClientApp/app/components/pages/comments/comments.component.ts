﻿import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, AbstractControl } from "@angular/forms";

import { StorageService } from "../../shared/services/storage.service";
import { SecurityService } from "../../shared/services/security.service";
import { CommentsService, CommentItem, CreateComment } from "./comments.service";

const Comments: OldComment[] = [{
    name: 'Сергей',
    comment: 'Очень доброжелательный курьер - выпишите ему премию). К мясу вопросов нет.',
    date: new Date(2018, 6, 16, 16, 24).toISOString(),
}, {
    name: 'Елизавета',
    comment: 'Устроила мужу сюрприз, заказала комбо - все вкусно',
    date: new Date(2018, 6, 14, 20, 53).toISOString(),
}];

@Component({
    templateUrl: 'comments.component.html',
    styleUrls: ['comments.component.scss'],
    providers: [CommentsService]
})
export class CommentsComponent implements OnInit {
    form: FormGroup;
    comments: CommentItem[] = [];
    isAuthenficated = false;
    commentSended = false;

    constructor(private storageService: StorageService,
        private securityService: SecurityService,
        private commentsService: CommentsService) {

        this.form = new FormGroup({
            'name': new FormControl('', [
                Validators.required,
            ]),
            'comment': new FormControl('', Validators.required)
        });

        this.securityService.settingsLoaded$.subscribe(() => {
            this.loadSettings();
        });

        this.loadSettings();
    }

    ngOnInit(): void {
        this.initComments();
    }

    addComment() {
        let comment: CreateComment = {
            Name: this.name!.value,
            Text: this.comment!.value,
            CreatedOn: new Date().getTime() / 1000,
        };

        this.commentsService.createComment(comment)
            .subscribe(() => {
                this.form.reset();
                this.loadSettings();

                this.initComments();
                this.commentSended = true;
            });
    }

    get name(): AbstractControl {
        return this.form.get('name')!; 
    }

    get comment(): AbstractControl {
        return this.form.get('comment')!;
    }

    private initComments(): void {
        this.commentsService.get()
            .subscribe(m => this.comments = m);
    }

    private loadSettings(): void {
        this.isAuthenficated = this.securityService.isAuthenficated;
        let clearName = true;
        let nameControl = this.form.get('name')!;

        if (this.isAuthenficated) {
            let name = this.securityService.profile!.Name;
            if (name) {
                clearName = false;
                nameControl.setValue(name);
                nameControl.disable();
            }
        }

        if (clearName) {
            nameControl.setValue('');
            nameControl.enable();
        }
    }
}

interface OldComment {
    name: string;
    date: string,
    comment: string;
}