﻿import { Component } from "@angular/core";
import { PhoneNumber, PhoneNumberCall } from "../../shared/constants";
import { ConfigurationService } from "../../shared/services/configuration.service";

@Component({
    templateUrl: './contacts.component.html',
    styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {
    beginWorkTime: string;
    endWorkTime: string;
    phoneNumber = PhoneNumber;
    phoneNumberCall = PhoneNumberCall

    constructor(private configurationService: ConfigurationService) {
        this.beginWorkTime = this.configurationService.serverSettings.StartTime;
        this.endWorkTime = this.configurationService.serverSettings.EndTime;
    }
}