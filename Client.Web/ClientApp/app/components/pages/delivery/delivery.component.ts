﻿import { Component, OnDestroy } from "@angular/core";
import { ConfigurationService } from "../../shared/services/configuration.service";
import { Subscription } from "rxjs";

@Component({
    templateUrl: './delivery.component.html',
    styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnDestroy {
    nonDeliveryDiscount: number = 0;
    minPrice = 0;
    minNonDeliveryPrice = 0;
    startTime: string = '';
    endTime: string = '';
    subscription: Subscription | null = null;

    constructor(private configurationService: ConfigurationService) {
        this.updateSettings();
        this.subscription = this.configurationService.settingsLoaded$.subscribe(() => this.updateSettings());
    }

    ngOnDestroy() {
        if (this.subscription)
            this.subscription.unsubscribe();
    }

    private updateSettings() {
        this.minPrice = this.configurationService.serverSettings.MinPrice;
        this.minNonDeliveryPrice = this.configurationService.serverSettings.MinNonDeliveryPrice;
        this.nonDeliveryDiscount = this.configurationService.serverSettings.NonDeliveryDiscount;
        this.startTime = this.configurationService.serverSettings.StartTime;
        this.endTime = this.configurationService.serverSettings.EndTime;
    }
}