import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { ConfigurationService } from '../shared/services/configuration.service';
import { LocationService } from '../shared/services/location.service';
import { SecurityService } from '../shared/services/security.service';
import { YaService } from '../shared/services/ya.service';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    showLocation: boolean = false;

    constructor(private configurationService: ConfigurationService,
        private locationService: LocationService,
        private yaService: YaService,
        private securityService: SecurityService,
        private router: Router) {
    }

    ngOnInit() {
        //Get configuration from server environment variables:
        this.securityService.load();

        this.locationService.locationNeeded$
            .subscribe(() => this.showLocation = true);

        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            if (typeof window !== 'undefined') {
                if (evt.url.indexOf('#') === -1) {
                    window.scrollTo(0, 0);
                }
            }
        });
    }

    hideLocation(): void {
        this.showLocation = false;
    }
}
