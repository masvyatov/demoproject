﻿import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";
import { Subscription } from "rxjs";
import { Observable } from "rxjs/Observable";

import { OrderService } from "./order.service";
import { IOrder } from "./order.model";

type DateType = 'today' | 'tomorrow';
const PaymentType = {
    ByCard: 1,
    ByMoney: 2
};

@Component({
    selector: 'order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
    providers: [OrderService, DatePipe]
})
export class OrderComponent implements OnInit {
    orderInfo: IOrder | null = null;
    orderId: string = '';

    constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private orderService: OrderService) {
    }

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe(params => {
            this.orderId = params['OrderId'];

            this.orderService.getOrder(this.orderId)
                .catch(error => {
                    this.navigateHome();
                    return Observable.throw(error);
                })
                .subscribe(order => {
                    if (order == null) {
                        this.navigateHome();
                    }
                    this.orderInfo = order;
                });
        });
    }

    navigateHome(): void {
        this.router.navigateByUrl('/');
    }

    get DeliveryTime(): Date | null {
        if (!this.orderInfo)
            return null;

        return new Date(this.orderInfo.DeliveryTime * 1000);
    }

    toMenu(): void {
        this.router.navigate(['/']);
    }
}
