﻿import { Injectable } from "@angular/core";
import { Http, RequestOptionsArgs, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

import { ConfigurationService } from "../shared/services/configuration.service";
import { IOrder, CreateOrderResult, CreateOrderModel, IOrderDelivery } from "./order.model";

@Injectable()
export class OrderService {
    constructor(private configurationService: ConfigurationService,
        private http: Http) { }

    createOrder(model: CreateOrderModel): Observable<CreateOrderResult> {
        const url = this.getProductUrl() + '/api/order/createorder';
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getOrder(orderId: string): Observable<IOrder> {
        const url = this.getProductUrl() + '/api/order/getorder';
        const model = { orderId: orderId };
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getOrderDeliveries(): Observable<IOrderDelivery[]> {
        const url = this.getProductUrl() + '/api/lead/getdeliveries';
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, {}, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getOrderNumber(): Observable<number> {
        const url = this.getProductUrl() + '/api/order/OrderNumber';
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, {}, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    applyPromo(promo: string): Observable<number | null> {
        const url = this.getProductUrl() + '/api/order/ApplyPromo';
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, { PromoCode: promo }, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getProductUrl(): string {
        return this.configurationService.serverSettings.ProductUrl;
    }
}