﻿export interface IOrder {
    Number: number;
    Items: IOrderItem[];
    Delivery: IOrderDelivery;
    DeliveryTime: number;
    Amount: number;
}

export interface IOrderDelivery {
    LeadDeliveryId: string;
    Name: string;
    StreetName: string;
    BuildingName: string;
    BuildingId: string;
}

export interface IOrderItem {
    Name: string;
    Count: number;
    Amount: number;
}

export interface CreateOrderModel {
    DeliveryType: number;
    LeadName: string;
    PaymentType: number;
    Comment: string;
    Amount: number;
    Bonus: number;
    DeliveryTime: number;
    OrderDeliveryId: string | null;
    CreateLeadDelivery: CreateLeadDeliveryModel | null;
    PaymentSum: number | null;
    Discount: number;
    PromoCode: string;
    Items: OrderProductItem[];
}

export interface CreateLeadDeliveryModel {
    Apartment: string;
    BuildingId: string;
    DoorCode: string;
    Entance: string;
    Floor: string;
    House: string;
    Name: string;
    Street: string;
}

export interface OrderProductItem {
    ProductId: string;
    Amount: number;
    Count: number;
}

export interface CreateOrderResult {
    Success: boolean;
    RedirectUrl: string | null;
    OrderId: string;
}
