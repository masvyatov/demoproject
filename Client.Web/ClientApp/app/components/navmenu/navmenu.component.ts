﻿import { Component, OnInit, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { PhoneNumber, getGroupNameId, Groups, PhoneNumberShort, PhoneNumberCall } from '../shared/constants';
import { SecurityService } from '../shared/services/security.service';
import { OrderService } from '../order/order.service';
import { ConfigurationService } from '../shared/services/configuration.service';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.scss'],
    providers: [OrderService]
})
export class NavMenuComponent implements OnInit, AfterViewInit, OnDestroy {
    fakeHeight = 120;
    private settingsSubscription: Subscription | null = null;

    isAuthenficated: boolean = false;

    phoneNumber = PhoneNumber;
    phoneNumberShort = PhoneNumberShort;
    phoneNumberCall = PhoneNumberCall;

    beginWorkTime: string;
    endWorkTime: string;

    groups = Groups;

    constructor(private securityService: SecurityService,
        private configurationService: ConfigurationService,
        private router: Router) {
        this.beginWorkTime = configurationService.serverSettings.StartTime;
        this.endWorkTime = configurationService.serverSettings.EndTime;
    }

    ngOnInit() {
        this.settingsSubscription = this.securityService.settingsLoaded$.subscribe(() => {
            this.isAuthenficated = this.securityService.isAuthenficated;
        });
    }

    ngAfterViewInit() {
        this.onWindowScroll();
    }

    ngOnDestroy() {
        if (this.settingsSubscription != null) {
            this.settingsSubscription.unsubscribe();
        }
    }

    toLogin(): void {
        this.router.navigate(['/login']);
    }

    toProfile(): void {
        this.router.navigate(['/profile']);
    }

    @HostListener('document:click')
    onNavigate(): void {
        let classList = document.getElementById('navbarToggler')!.classList;
        classList.remove('show');
    }

    getGroupId(group: string): string {
        return getGroupNameId(group);
    }

    @HostListener("window:scroll", [])
    @HostListener("window:resize", [])
    onWindowScroll() {
        if (typeof window === 'undefined' || typeof document === 'undefined')
            return;

        this.fakeHeight = document.querySelector('.navbar')!.getBoundingClientRect().height;
    }
}
