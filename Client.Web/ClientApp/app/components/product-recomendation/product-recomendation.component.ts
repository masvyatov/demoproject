﻿import { Component, OnInit, OnDestroy } from "@angular/core";

import { Subscription } from "rxjs/Subscription";

import { BasketService } from "../shared/services/basket.service";
import { ProductService } from "../product-item/product.service";

import { Product } from "../product-item/product.data";
import { BasketItem } from "../shared/models/basket.model";

@Component({
    selector: 'product-recomendation',
    templateUrl: './product-recomendation.component.html',
    styleUrls: ['../product-free/product-free.component.scss']
})
export class ProductRecomendationComponent implements OnInit, OnDestroy {
    products: Product[] = [];
    basketSubscription: Subscription | null = null;

    constructor(private basketService: BasketService,
        private productService: ProductService) { }

    ngOnInit(): void {
        this.onBasketChanged();
        this.basketSubscription = this.basketService.basket.changed$.subscribe(() => {
            this.onBasketChanged();
        });
    }

    ngOnDestroy(): void {
        if (this.basketSubscription)
            this.basketSubscription.unsubscribe();
    }

    private onBasketChanged(): void {
        this.productService.getProductList()
            .subscribe(products => {
                let ids: string[] = [];
                for (let item of this.basketService.basket.Items) {
                    let product = products.filter(m => m.ProductId === item.ProductId)[0];
                    if (!product)
                        continue;

                    for (let rec of product.Recomendations) {
                        if (ids.indexOf(rec) === -1) {
                            ids.push(rec);
                        }
                    }
                }

                this.products = products.filter(m => ids.indexOf(m.ProductId) !== -1)
                    .map(product => ProductService.toProductList(product))
                    .sort((a, b) => {
                        if (a.Name < b.Name)
                            return -1;
                        if (a.Name > b.Name)
                            return 1;
                        return 0;
                    });
            });
    }

    count(product: Product): number {
        let products = this.basketService.basket.Items.filter(m => m.ProductId === product.ProductId);
        if (products.length) {
            return products[0].Count;
        }

        return 0;
    }

    addToCard(product: Product): void {
        let count = this.count(product);
        if (count >= 99)
            return;

        if (product && product.SaleCount != null && product.SaleCount <= count)
            return;

        count++;
        this.updateBasket(product, count);
    }

    removeFromCard(product: Product): void {
        let count = this.count(product);
        count--;
        this.updateBasket(product, count);
    }

    private updateBasket(product: Product, count: number): void {
        const basketItem: BasketItem = new BasketItem(product.ProductId,
            product.Name,
            count,
            this.getPrice(product));

        this.basketService.update(basketItem)
            .subscribe();
    }

    getPrice = (product: Product): number => ProductService.getProductPrice(product) || 0;
}
