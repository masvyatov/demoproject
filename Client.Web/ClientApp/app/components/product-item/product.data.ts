﻿export interface Product {
    ProductId: string;
    Name: string;
    GroupName: string;
    Description: string;
    FullDescription: string;
    Info: string | null;
    ImageUrls: string[];
    SmallImageUrl: string;
    Price: number;
    SalePrice: number | null;
    Count: number;
    Height: number;
    Quantity: number;
    Unit: string;
    IsForSale: boolean;
    Sequence: number;
    SaleCount: number | null;
}

export interface IProduct {
    ProductId: string;
    Name: string;
    GroupName: string;
    Price: number;
    SalePrice: number | null;
    Description: string;
    FullDescription: string;
    Height: number;
    Quantity: number;
    Unit: string;
    IsForSale: boolean;
    Sequence: number;
    SaleCount: number | null;
    CanFree: boolean;

    Recomendations: string[];
    ImageIds: string[];
}

export interface ProductList {
    Name: string;
    GroupName: string;
    Sequence: number;

    Products: Product[];
}

export interface ProductGroupList {
    GroupName: string;
    Items: ProductList[];
}