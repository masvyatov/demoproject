﻿import { Injectable, EventEmitter } from "@angular/core";
import { Http } from "@angular/http";
import { Response } from '@angular/http';
import { Observable } from "rxjs/Observable";

import { ConfigurationService } from "../shared/services/configuration.service";
import { IProduct, ProductGroupList, ProductList, Product } from "./product.data";
import { Groups, cyrillicToTranslit, StoreId } from "../shared/constants";

@Injectable()
export class ProductService {
    private stamp: Date | null = null;
    private products: IProduct[] = [];
    private listExecuting: boolean = false;
    private listEmmiter: EventEmitter<IProduct[]> = new EventEmitter();

    constructor(private configurationService: ConfigurationService,
        private http: Http) {
    }

    getProductList(): Observable<IProduct[]> {
        return this.getProductListInternal();
    }

    getProductGroupList(): Observable<ProductGroupList[]> {
        return this.getProductList()
            .map(value => {
                const productLists: ProductList[] = [];
                for (let iproduct of value) {
                    let product = ProductService.toProductList(iproduct);
                    let filterProductLists = productLists.filter(m => m.Name == product.Name);

                    if (filterProductLists.length > 0) {
                        filterProductLists[0].Products.push(product);
                        filterProductLists[0].Sequence = filterProductLists[0].Sequence || product.Sequence;
                    }
                    else {
                        let productList: ProductList = {
                            GroupName: product.GroupName,
                            Name: product.Name,
                            Products: [product],
                            Sequence: product.Sequence,
                        };
                        productLists.push(productList);
                    }
                }

                const productGroupLists: ProductGroupList[] = [];

                for (let list of productLists) {
                    list.Products.sort((a, b) => a.Height - b.Height);

                    let possible = productGroupLists.filter(m => m.GroupName === list.GroupName)[0];
                    if (possible) {
                        possible.Items.push(list);
                    }
                    else {
                        possible = {
                            GroupName: list.GroupName,
                            Items: [list],
                        };
                        productGroupLists.push(possible);
                    }
                }

                productGroupLists.sort((a, b) => this.groupWeight(a.GroupName) - this.groupWeight(b.GroupName));

                for (let groupList of productGroupLists) {
                    groupList.Items.sort((a, b) => a.Sequence - b.Sequence);
                }

                return productGroupLists;
            });
    }

    private groupWeight(groupName: string): number {
        let index = Groups.indexOf(groupName);
        return index === -1 ? 100 : index;
    }

    static toProductList(product: IProduct): Product {
        return {
            Count: 0,
            ProductId: product.ProductId,
            Description: product.Description,
            FullDescription: product.FullDescription,
            GroupName: product.GroupName,
            Name: product.Name,
            ImageUrls: this.getImageUrls(product.ImageIds),
            SmallImageUrl: this.getSmallImageUrl(product.ImageIds),
            Info: null,
            Price: product.Price,
            SalePrice: product.SalePrice,
            Height: product.Height,
            Quantity: product.Quantity,
            Unit: product.Unit,
            IsForSale: product.IsForSale,
            Sequence: product.Sequence,
            SaleCount: product.SaleCount,
        };
    }

    private getProductListInternal(): Observable<IProduct[]> {
        let now = new Date();

        if (this.stamp != null && (now.getTime() - this.stamp.getTime()) < 1000 * 60 * 5) {
            return new Observable((resolver) => {
                resolver.next(this.products);
            });
        }

        if (this.listExecuting) {
            return new Observable((resolver) => {
                this.listEmmiter.first().subscribe((products: IProduct[]) => {
                    resolver.next(products);
                });
            });
        }

        this.listExecuting = true;
        return this.getProductListStorageInternal()
            .map(products => {
                this.products = products;
                this.stamp = now;
                this.listExecuting = false;
                this.listEmmiter.emit(products);
                return products;
            })
            .catch(err => {
                this.stamp = null;
                this.listExecuting = false;
                return Observable.throw(err);
            });
    }

    private getProductListStorageInternal(): Observable<IProduct[]> {
        const url = this.getProductUrl() + '/api/product/sitelist/' + StoreId;
        return this.http.get(url)
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    private getProductUrl(): string {
        return this.configurationService.serverSettings.ProductUrl;
    }

    static getProductUrl(product: Product | null): string[] {
        if (product == null)
            return ['/'];

        return ['/prod', cyrillicToTranslit(product.Name, '_').toLowerCase(), product.ProductId];
    }

    static getImageUrls(imageIds: string[]): string[] {
        if (!imageIds || imageIds.length == 0)
            return ["/images/product/default.jpeg"];
        return imageIds.map(p => `/images/product/${p}.jpeg`);
    }

    static getSmallImageUrl(imageIds: string[]): string {
        if (!imageIds || imageIds.length == 0)
            return "/images/product/default.jpeg";
        return imageIds.map(p => `/images/product/${p}_sm.jpeg`)[0];
    }

    static getProductPrice(product: Product | null): number | null {
        if (product == null)
            return null;

        if (product.SalePrice != null && product.SalePrice < product.Price) {
            return product.SalePrice;
        }

        return product.Price;
    }

    static isForSale(product: Product | null): boolean {
        return product != null && ProductService.getProductPrice(product) != product.Price;
    }
}
