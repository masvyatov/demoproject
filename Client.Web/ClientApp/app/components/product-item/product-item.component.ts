﻿import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Product, ProductList } from './product.data';
import { BasketService } from '../shared/services/basket.service';
import { BasketItem } from '../shared/models/basket.model';
import { Subscription } from 'rxjs';
import { ProductService } from './product.service';

@Component({
    selector: 'product-item',
    templateUrl: './product-item.component.html',
    styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit, OnDestroy {
    basketSubsription: Subscription | null = null;
    product: Product | null = null;
    @Input() productList: ProductList | null = null;

    count = 0;

    heights: number[] = [];
    currentHeight: number = 0;

    quantities: number[] = [];
    currentQuantity: number = 0;

    constructor(private basketService: BasketService) { }

    ngOnInit(): void {
        this.setProduct(this.productList!.Products[0]);
        this.initSizes();

        const basket = this.basketService.basket;
        this.onBasketChange();
        this.basketSubsription = basket.changed$.subscribe(() => {
            this.onBasketChange();
        });
    }

    ngOnDestroy(): void {
        if (this.basketSubsription != null)
            this.basketSubsription.unsubscribe();
    }

    addToCard(): void {
        if (this.count >= 99)
            return;

        if (this.product && this.product.SaleCount != null && this.product.SaleCount <= this.count)
            return;

        this.count++;
        this.updateBasket();
    }

    removeFromCard(): void {
        this.count--;
        this.updateBasket();
    }

    private updateBasket() {
        if (this.product == null)
            return;

        const basketItem: BasketItem = new BasketItem(this.product.ProductId,
            this.product.Name,
            this.count,
            this.getPrice() || 0);

        this.basketService.update(basketItem)
            .subscribe();
    }

    private initSizes(): void {
        if (this.productList == null)
            return;

        this.quantities = this.productList.Products
            .map(m => m.Quantity)
            .filter((v, i, a) => a.indexOf(v) === i);
        this.currentQuantity = this.product!.Quantity;

        this.rebuildHeights();
    }

    private rebuildHeights() {
        this.heights = this.productList!.Products
            .filter(m => m.Quantity == this.currentQuantity)
            .map(m => m.Height)
            .filter((v, i, a) => a.indexOf(v) === i);
        this.currentHeight = this.product!.Height;
    }

    private onBasketChange(): void {
        const basket = this.basketService.basket;
        const product = this.product;

        if (product != null) {
            let products = basket.Items.filter(m => m.ProductId == product!.ProductId);
            let count = 0;
            for (let i = 0; i < products.length; i++) {
                count += products[i].Count;
            }

            this.count = count;
        }
    }

    setProduct(product: Product): void {
        this.product = product;
        this.onBasketChange();
    }

    changeQuantity(quantity: number) {
        let product = this.productList!.Products.find(m => m.Height == this.currentHeight && m.Quantity == quantity);
        this.currentQuantity = quantity;
        this.setProduct(product!);
        this.rebuildHeights();
    }

    changeHeight(height: number): void {
        let product = this.productList!.Products.find(m => m.Height == height && m.Quantity == this.currentQuantity);
        this.currentHeight = height;
        this.setProduct(product!);
    }

    getProductLink(): string[] {
        return ProductService.getProductUrl(this.product);
    }

    getProductImageUrl(): string {
        return this.product!.ImageUrls[0];
    }

    isSale = () => ProductService.isForSale(this.product);

    getPrice = () => ProductService.getProductPrice(this.product);
}
