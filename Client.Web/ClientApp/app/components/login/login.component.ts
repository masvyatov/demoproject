﻿import { Component, AfterViewInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { Observable } from "rxjs/Observable";
import { LoginService } from "./login.service";
import { SecurityService } from "../shared/services/security.service";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit, OnDestroy {
    form: FormGroup;
    phoneMask = ['\+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    codeMask = [/\d/, /\d/, /\d/, /\d/];

    isCodeSended: boolean = false;
    countDown: number = 0;
    title = 'Вход на сайт';

    @ViewChild("phoneRef") phoneRef: ElementRef | null = null;
    @ViewChild("codeRef") codeRef: ElementRef | null = null;

    constructor(private loginService: LoginService,
        private securityService: SecurityService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder) {
        this.form = this.fb.group({
            phone: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]7 \(9[0-9]{2}\) [0-9]{3}-[0-9]{4}$/)])],
            code: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]{4}$/)])]
        });

        let queryParams = this.activatedRoute.snapshot.queryParams;
        if (queryParams['returnUrl'] === '/checkout') {
            this.title = 'Для совершения заказа войдите на сайт';
        }
    }

    ngAfterViewInit(): void {
        this.phoneRef!.nativeElement.focus();
        this.onPhoneFocus();
    }

    ngOnDestroy(): void { }

    onPhoneFocus(): void {
        setTimeout(() => {
            const value: string = this.phone!.value;
            let selectPosition = 4;
            if (value) {
                let index = value.indexOf('_');
                if (index !== -1) {
                    selectPosition = index;
                }
                else {
                    return;
                }
            }
            const elem = this.phoneRef!.nativeElement;
            if (elem.setSelectionRange) {
                elem.setSelectionRange(selectPosition, selectPosition);
            }
        }, 0);
    }

    get phone(): AbstractControl | null {
        return this.form.get('phone');
    }

    get code(): AbstractControl | null {
        return this.form.get('code');
    }

    sendCode(): void {
        if (!this.phone!.disabled) {
            this.isCodeSended = true;
            this.setCountDown();
            this.loginService.sendCode(this.phone!.value)
                .subscribe(() => {
                    this.codeRef!.nativeElement.focus();
                });
        }
    }

    setCountDown(): void {
        Observable.timer(0, 1000)
            .map(value => 60 - value)
            .takeWhile(value => value >= 0)
            .subscribe(val => this.countDown = val);
    }

    login(): void {
        if (!this.code!.invalid) {
            this.loginService.login(this.phone!.value, this.code!.value)
                .subscribe(val => {
                    if (val === true) {
                        this.securityService.load();
                        this.activatedRoute.queryParams.subscribe(params => {
                            const url: string = params['returnUrl'] || '/';
                            this.router.navigateByUrl(url);
                        });
                    }
                });
        }
    }

    onPastePhone(e: any): void {
        let value = e.clipboardData.getData("Text");
        if (value && value.startsWith('+')) {
            value = value.substring(1);
        }
        if (value && value.startsWith('7')) {
            value = value.substring(1);
        }

        this.phone!.setValue(value);
        this.phone!.updateValueAndValidity();
    }
}
