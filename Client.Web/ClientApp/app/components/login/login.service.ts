﻿import { Injectable } from "@angular/core";
import { Http, RequestOptionsArgs, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

import { ConfigurationService } from "../shared/services/configuration.service";

@Injectable()
export class LoginService {
    constructor(private http: Http,
        private configurationService: ConfigurationService) { }

    sendCode(phone: string): Observable<void> {
        const url = this.getProductUrl() + '/api/lead/sendcode';
        const model = { phone: phone };
        let options: RequestOptionsArgs = {};
        let body = JSON.stringify(model);

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                return;
            });
    }

    login(phone: string, code: string): Observable<boolean> {
        const url = this.getProductUrl() + '/api/lead/login';
        const model = { phone: phone, code: code };
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getProductUrl(): string {
        return this.configurationService.serverSettings.ProductUrl;
    }
}