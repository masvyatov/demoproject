﻿import { Component, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";

import { ProductService } from "../product-item/product.service";
import { BasketService } from "../shared/services/basket.service";
import { Product, ProductList } from "../product-item/product.data";
import { BasketItem } from "../shared/models/basket.model";

@Component({
    selector: 'product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnDestroy {
    product: Product | null = null;
    productList: ProductList | null = null;

    sizes: string[] = [];
    currentSize: string = '';

    basketSubsription: Subscription | null = null;
    count = 0;

    constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private productService: ProductService,
        private basketService: BasketService
    ) {
        activatedRoute.paramMap.subscribe(paramMap => {
            const id = paramMap.get("id");
            this.productService.getProductGroupList()
                .subscribe(resp => {
                    let finded = false;
                    for (let group of resp) {
                        for (let item of group.Items) {
                            for (let product of item.Products) {
                                if (product.ProductId == id) {
                                    this.productList = item;
                                    this.product = product;
                                    finded = true;
                                    break;
                                }
                            }
                            if (finded)
                                break;
                        }
                        if (finded)
                            break;
                    }

                    if (!finded) {
                        router.navigateByUrl('/');
                    }
                    else {
                        this.initSizes();
                    }

                    this.ngOnDestroy();
                    const basket = this.basketService.basket;
                    this.onBasketChange();
                    this.basketSubsription = basket.changed$.subscribe(() => {
                        this.onBasketChange();
                    });
                });
        });
    }

    ngOnDestroy() {
        if (this.basketSubsription) {
            this.basketSubsription.unsubscribe();
        }
    }

    private initSizes(): void {
        if (this.productList == null)
            return;

        this.sizes = this.productList.Products
            .map(m => m.Height + this.getProductSizeSuffix(m));
        this.currentSize = this.product!.Height + this.getProductSizeSuffix(<Product>this.product);
    }

    private getProductSizeSuffix(product: Product): string {
        return ` ${product.Unit}`;
    }

    changeSize(size: string): void {
        let products = this.productList!.Products.filter(m => m.Height + this.getProductSizeSuffix(m) == size);
        let setProduct = products[0];
        if (setProduct) {
            this.router.navigate(ProductService.getProductUrl(setProduct));
        }
    }

    addToCard(): void {
        if (this.count >= 99)
            return;

        if (this.product && this.product.SaleCount != null && this.product.SaleCount <= this.count)
            return;

        this.count++;
        this.updateBasket();
    }

    removeFromCard(): void {
        this.count--;
        this.updateBasket();
    }

    private updateBasket() {
        if (this.product == null)
            return;

        const basketItem: BasketItem = new BasketItem(this.product.ProductId,
            this.product.Name,
            this.count,
            this.product.Price);

        this.basketService.update(basketItem)
            .subscribe();
    }

    private onBasketChange(): void {
        const basket = this.basketService.basket;
        const product = this.product;

        if (product != null) {
            let products = basket.Items.filter(m => m.ProductId == product!.ProductId);
            let count = 0;
            for (let i = 0; i < products.length; i++) {
                count += products[i].Count;
            }

            this.count = count;
        }
    }

    getPrice = (): number => ProductService.getProductPrice(this.product) || 0;

    isSale = () => ProductService.isForSale(this.product);
}