import { Component } from '@angular/core';

import { PhoneNumber, PhoneNumberCall } from '../shared/constants';
import { ConfigurationService } from '../shared/services/configuration.service';

@Component({
    selector: 'footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
    phoneNumber = PhoneNumber;
    phoneNumberCall = PhoneNumberCall;
    beginWorkTime: string;
    endWorkTime: string;

    constructor(configurationService: ConfigurationService) {

        this.beginWorkTime = configurationService.serverSettings.StartTime;
        this.endWorkTime = configurationService.serverSettings.EndTime;
    }
}
