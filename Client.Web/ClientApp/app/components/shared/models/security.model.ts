﻿export class DeliveryHouse {
    StreetName: string = '';
    House: string = '';
}

export interface Profile {
    Name: string;
    Phone: string;
    Bonus: number;
}