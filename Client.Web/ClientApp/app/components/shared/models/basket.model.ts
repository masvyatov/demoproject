﻿import { EventEmitter } from "@angular/core";

import { IProduct } from "../../product-item/product.data";
import { ProductService } from "../../product-item/product.service";

export class BasketItem {
    ProductId: string;
    Name: string;
    Count: number;
    Price: number;
    Amount: number;
    Height: number = 0;
    Quantity: number = 0;
    IsForSale: boolean = true;
    SaleCount: number | null = null;

    constructor(productId: string,
        name: string,
        count: number,
        price: number
    ) {
        this.ProductId = productId;
        this.Name = name;
        this.Count = count;
        this.Price = price;
        this.Amount = count * price;
    }

    get isValidCount(): boolean {
        return this.SaleCount == null || this.SaleCount >= this.Count;
    }

    canAddProduct(): boolean {
        return this.SaleCount == null || this.SaleCount > this.Count;
    }
}

export class BasketFreeItem {
    InBasket: number = 0;
    Count: number = 0;

    copyFrom(item: BasketFreeItem): void {
        this.InBasket = item.InBasket;
        this.Count = item.Count;
    }
}

export class Basket {
    changed$: EventEmitter<void> = new EventEmitter();
    Items: BasketItem[] = [];
    Amount: number = 0;
    Count: number = 0;

    constructor() { }

    update(products: IProduct[], item: BasketItem): void {
        let currentItem = this.Items.filter(m => {
            return m.ProductId === item.ProductId;
        });

        if (currentItem.length === 0) {
            this.Items.push(item);
        }
        else {
            const index = this.Items.indexOf(currentItem[0]);
            if (item.Count === 0) {
                this.Items.splice(index, 1);
            }
            else {
                this.Items.splice(index, 1, item);
            }
        }

        this.recalculate(products);
        this.changed$.emit();
    }

    clear(): void {
        this.Items = [];
        this.Amount = 0;
        this.Count = 0;
        this.changed$.emit();
    }

    private recalculate(products: IProduct[]): void {
        const productMap = new Map<string, IProduct>();
        for (let prod of products) {
            productMap.set(prod.ProductId, prod);
        }

        // names
        for (let item of this.Items) {
            let product = productMap.get(item.ProductId);
            if (product) {
                item.Name = product.Name;
                item.IsForSale = product.IsForSale;
                item.SaleCount = product.SaleCount;
                item.Height = product.Height;
                item.Quantity = product.Quantity;
            }
            else {
                item.IsForSale = false;
            }
        }

        // price
        for (let item of this.Items) {
            let product = productMap.get(item.ProductId);
            if (product) {
                item.Price = this.getPrice(product);
                let count = item.Count;
                item.Amount = item.Price * count;
            }
        }


        let amount = 0,
            count = 0;

        this.Items.forEach((value) => {
            amount += value.Amount;
            count += value.Count;
        });

        this.Amount = amount;
        this.Count = count;
    }

    private getPrice(product: IProduct): number {
        return ProductService.getProductPrice(ProductService.toProductList(product)) || 0;
    }
}
