﻿export interface IConfiguration {
    ProductUrl: string;
    LocationUrl: string;
    MinPrice: number;
    MaxPrice: number;
    MaxPriceByMoney: number;
    StartTime: string;
    EndTime: string;
    DeliveryMinTime: number;
    NonDeliveryDiscount: number;
    WorkingToday: boolean;
    MinNonDeliveryPrice: number;
}

export interface IServerConfiguration {
    locationUrl: string;
    maxPrice: number;
    maxPriceByMoney: number;
    minPrice: number;
    minNonDeliveryPrice: number;
    productUrl: string;

    startTime: string;
    endTime: string;
    deliveryMinTime: number;
    nonDeliveryDiscount: number;
    workingToday: boolean;
}