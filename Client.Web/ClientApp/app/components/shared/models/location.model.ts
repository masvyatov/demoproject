﻿export interface IBuildingModel {
    Name: string;
    Id: string;
    Avalable: boolean;
}

export interface IStreetModel {
    CityName: string;
    StreetName: string;
    StreetId: string;
    Avalable: boolean;
}

export class DeliveryLocation {
    constructor(CityName: string,
        StreetName: string,
        BuildingName: string,
        BuildingId: string) { }
}