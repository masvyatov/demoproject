﻿import { Injectable } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

declare var yaCounter49000691: any;
declare var ga: any;

@Injectable()
export class YaService {
    private targets: string[] = [];

    constructor(private router: Router) {
        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
                this.hit(evt.url);
            }
        });
    }

    reachGoal(target: string, once: boolean = true): void {
        // target once
        if (once && this.targets.indexOf(target) !== -1)
            return;

        if (this.ifYa()) {
            yaCounter49000691.reachGoal(target);
        }
    }

    hit(url: string): void {
        if (this.ifYa()) {
            yaCounter49000691.hit(url);
        }

        if (typeof ga !== 'undefined') {
            ga('set', 'page', url);
            ga('send', 'pageview');
        }
    }

    ifYa(): boolean {
        return typeof yaCounter49000691 !== 'undefined';
    }
}