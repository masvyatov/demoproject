﻿import { Injectable } from "@angular/core";
import { Http, Response, RequestOptionsArgs, Headers } from "@angular/http";

import { Observable } from "rxjs/Observable";
import { DeliveryHouse, Profile } from "../models/security.model";
import { ConfigurationService } from "./configuration.service";
import { Subject } from "rxjs";

@Injectable()
export class SecurityService {
    isAuthenficated: boolean = false;
    hasDelivery: boolean = false;
    profile: Profile | null = null;

    deliveryHouse: DeliveryHouse | null = null;

    private settingsLoadedSource = new Subject();
    settingsLoaded$ = this.settingsLoadedSource.asObservable();

    constructor(private http: Http,
        private configurationService: ConfigurationService) { }

    load(): void {
        const url = this.getProductUrl() + '/api/lead/profile';
        const model = {};
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            }).subscribe(val => {
                this.isAuthenficated = val != null;
                this.profile = val;
                this.settingsLoadedSource.next();
            });
    }

    save(model: Profile): Observable<any> {
        this.profile!.Name = model.Name;

        const url = this.getProductUrl() + '/api/lead/saveprofile';
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    logout(): Observable<any> {
        this.profile = null;
        this.isAuthenficated = false;
        this.settingsLoadedSource.next();

        const url = this.getProductUrl() + '/api/lead/logout';
        const model = {};
        let options: RequestOptionsArgs = {};

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        options.withCredentials = true;

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getProductUrl(): string {
        return this.configurationService.serverSettings.ProductUrl;
    }
}