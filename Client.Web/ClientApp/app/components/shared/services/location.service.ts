﻿import { Injectable, EventEmitter } from "@angular/core";
import { Http, Response, RequestOptionsArgs, Headers } from "@angular/http";

import { Observable } from "rxjs/Observable";

import { ConfigurationService } from "./configuration.service";
import { IStreetModel, IBuildingModel, DeliveryLocation } from "../models/location.model";
import { StorageService } from "./storage.service";

@Injectable()
export class LocationService {
    locationChanged$: EventEmitter<void> = new EventEmitter();
    locationNeeded$: EventEmitter<void> = new EventEmitter();
    currentDelivery: DeliveryLocation | null = null;

    constructor(private http: Http,
        private configurationService: ConfigurationService,
        private storageService: StorageService) {
        this.readDelivery();
    }

    getStreets(name: string): Observable<IStreetModel[]> {
        const url = this.getLocationUrl() + '/api/location/streets';
        const model = { name: name };
        let options: RequestOptionsArgs = {};
        let body = JSON.stringify(model);

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getBuildings(name: string, streetId: string): Observable<IBuildingModel[]> {
        const url = this.getLocationUrl() + '/api/location/buildings';
        const model = { name: name, streetId: streetId };
        let options: RequestOptionsArgs = {};
        let body = JSON.stringify(model);

        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('X-Requested-With', 'XMLHttpRequest');

        return this.http.post(url, model, options)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    getLocationUrl(): string {
        return this.configurationService.serverSettings.LocationUrl;
    }

    setDeliveryLocation(location: DeliveryLocation): void {
        this.currentDelivery = location;
        this.writeDelivery();
    }

    private writeDelivery(): void {
        this.storageService.store('delivery', this.currentDelivery);
    }

    private readDelivery(): void {
        let delivery = this.storageService.retrieve('delivery');
        if (delivery) {
            this.currentDelivery = delivery;
        }
    }
}
