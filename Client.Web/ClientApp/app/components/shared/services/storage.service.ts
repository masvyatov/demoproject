﻿import { Injectable } from '@angular/core';

export interface IStorageService {
    retrieve(key: string): any;
    store(key: string, value: any): void;
}

@Injectable()
export class StorageService implements IStorageService {
    private storage: any;

    constructor() {
        this.storage = localStorage; // localStorage;
    }

    public retrieve(key: string): any {
        let item = this.storage.getItem(key);

        if (item && item !== 'undefined') {
            return JSON.parse(this.storage.getItem(key));
        }

        return;
    }

    public store(key: string, value: any): void {
        this.storage.setItem(key, JSON.stringify(value));
    }
}


class MemoryStorage {
    private set: Map<string, string> = new Map<string, string>();

    getItem(key: string): string | undefined {
        if (this.set.has(key))
            return this.set.get(key);

        return;
    }

    setItem(key: string, data: string): void {
        this.set.set(key, data);
    }
}

@Injectable()
export class InmemoryStorageService implements IStorageService {
    private storage = new MemoryStorage();

    public retrieve(key: string): any {
        let item = this.storage.getItem(key);

        if (item && item !== 'undefined') {
            return JSON.parse(item);
        }

        return;
    }

    public store(key: string, value: any) : void {
        this.storage.setItem(key, JSON.stringify(value));
    }
}
