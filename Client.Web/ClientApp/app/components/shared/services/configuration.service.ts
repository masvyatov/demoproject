﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { IConfiguration, IServerConfiguration } from '../models/configuration.model';

import { Subject } from 'rxjs/Subject';

@Injectable()
export class ConfigurationService {
    serverSettings: IConfiguration;

    // observable that is fired when settings are loaded from server
    private settingsLoadedSource = new Subject();
    settingsLoaded$ = this.settingsLoadedSource.asObservable();

    constructor(private http: Http,
        @Inject('SERVER_CONFIGURATION') serverConfiguration: IServerConfiguration,
        @Inject('ABSOLUTE_URL') absoluteUrl: string) {

        let additionalUrl = absoluteUrl.endsWith('/') ?
            absoluteUrl.substring(0, absoluteUrl.length - 1) : absoluteUrl;

        this.serverSettings = {
            LocationUrl: serverConfiguration.locationUrl,
            ProductUrl: serverConfiguration.productUrl,
            MaxPrice: serverConfiguration.maxPrice,
            MinPrice: serverConfiguration.minPrice,
            MaxPriceByMoney: serverConfiguration.maxPriceByMoney,
            DeliveryMinTime: serverConfiguration.deliveryMinTime,
            EndTime: serverConfiguration.endTime,
            StartTime: serverConfiguration.startTime,
            NonDeliveryDiscount: serverConfiguration.nonDeliveryDiscount,
            WorkingToday: serverConfiguration.workingToday,
            MinNonDeliveryPrice: serverConfiguration.minNonDeliveryPrice,
        };

        if (!this.serverSettings.LocationUrl.startsWith("http")) {
            this.serverSettings.LocationUrl = additionalUrl + this.serverSettings.LocationUrl;
        }

        if (!this.serverSettings.ProductUrl.startsWith("http")) {
            this.serverSettings.ProductUrl = additionalUrl + this.serverSettings.ProductUrl;
        }
    }

    // deprecated
    load() {
        if (typeof document === 'undefined')
            return;

        const baseURI = (document.baseURI || "").endsWith('/') ? document.baseURI : `${document.baseURI}/`;
        let url = `${baseURI}Home/Configuration`;
        this.http.get(url).subscribe((response: Response) => {
            console.log('server settings loaded');
            // TODO
            this.serverSettings = response.json();
            this.settingsLoadedSource.next();
        });
    }
}
