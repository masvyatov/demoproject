﻿import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";

import { Observable } from "rxjs/Observable";

import { StorageService } from "./storage.service";
import { ConfigurationService } from "./configuration.service";
import { ProductService } from "../../product-item/product.service";
import { YaService } from "./ya.service";
import { Basket, BasketItem } from "../models/basket.model";

@Injectable()
export class BasketService {
    basket: Basket = new Basket();

    constructor(private http: Http,
        private configurationService: ConfigurationService,
        private storageService: StorageService,
        private productService: ProductService,
        private yaService: YaService
    ) {
        this.readBasket();
    }

    update(item: BasketItem): Observable<boolean> {
        this.yaService.reachGoal('add_to_card');
        return this.updateInternal(item);
    }

    clear(): void {
        this.basket.clear();
        this.writeBasket();
    }

    private updateInternal(item: BasketItem): Observable<boolean> {
        this.updateBasketInternal(item);

        const model = {
            productId: item.ProductId,
            count: item.Count
        };

        const url = this.getProductUrl() + '/api/basket/update';
        return this.http.post(url, model)
            .catch((error) => {
                return Observable.throw(error);
            })
            .map((response: Response) => {
                if (response.status === 204) {
                    return null;
                }

                return response.json();
            });
    }

    private getProductUrl(): string {
        return this.configurationService.serverSettings.ProductUrl;
    }

    private writeBasket() {
        this.storageService.store("basket", this.basket.Items);
    }

    private updateBasketInternal(item: BasketItem) {
        this.productService.getProductList()
            .subscribe(products => {
                this.basket.update(products, item);
                this.writeBasket();
            });
    }

    private readBasket() {
        const items = this.storageService.retrieve("basket");
        if (items) {
            const itemsInBasket = <BasketItem[]>items;
            for (let i = 0; i < itemsInBasket.length; i++) {
                let iItem = itemsInBasket[i];
                const item = new BasketItem(iItem.ProductId, iItem.Name, iItem.Count, iItem.Price);
                item.SaleCount = iItem.SaleCount;
                item.IsForSale = iItem.IsForSale;
                this.updateBasketInternal(item);
            }
        }
    }
}
