﻿export const PhoneNumber = '8 831 235-17-72';
export const PhoneNumberShort = '235-17-72';
export const PhoneNumberCall = PhoneNumber.replace(/[^0-9]/g, '');
export const StoreId = '66ec5bcf-5b2f-4902-a7ce-c4d2bf859346';

export const MapScriptUrl = 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aca3c410b51e79da1c90757a7329e411bf7679989bed50a4ea252f458296e2c65&amp;width=100%25&amp;height=440&amp;lang=ru_RU&amp;scroll=false';

export const Drinks = 'Напитки';
export const Souces = 'Соусы';
export const Flatwares = 'Посуда';
const groupIdMap: { [key: string]: string } = { 'Шашлыки': 'charcoal', 'Ланч-боксы': 'lunch', 'Комбо': 'kombo', 'Закуски': 'snacks', 'Соусы': 'sauce', 'Напитки': 'drinks' };
export const Groups = ['Шашлыки', 'Закуски', 'Ланч-боксы', 'Комбо', Souces, Drinks];

export function getGroupNameId(name: string): string {
    return groupIdMap[name];
}

export function cyrillicToTranslit(input: string, spaceReplacement: string) {
    let _preset: string = "ru";

    const _firstLetterAssociations: { [key: string]: string } = {
        "а": "a",
        "б": "b",
        "в": "v",
        "ґ": "g",
        "г": "g",
        "д": "d",
        "е": "e",
        "ё": "e",
        "є": "ye",
        "ж": "zh",
        "з": "z",
        "и": "i",
        "і": "i",
        "ї": "yi",
        "й": "i",
        "к": "k",
        "л": "l",
        "м": "m",
        "н": "n",
        "о": "o",
        "п": "p",
        "р": "r",
        "с": "s",
        "т": "t",
        "у": "u",
        "ф": "f",
        "х": "h",
        "ц": "c",
        "ч": "ch",
        "ш": "sh",
        "щ": "sh'",
        "ъ": "",
        "ы": "i",
        "ь": "",
        "э": "e",
        "ю": "yu",
        "я": "ya",
    };

    if (_preset === "uk") {
        Object.assign(_firstLetterAssociations, {
            "г": "h",
            "и": "y",
            "й": "y",
            "х": "kh",
            "ц": "ts",
            "щ": "shch",
            "'": "",
            "’": "",
            "ʼ": "",
        });
    }

    const _associations = Object.assign({}, _firstLetterAssociations);

    if (_preset === "uk") {
        Object.assign(_associations, {
            "є": "ie",
            "ї": "i",
            "й": "i",
            "ю": "iu",
            "я": "ia",
        });
    }

    // transform

    if (!input) {
        return "";
    }

    let newStr = "";
    for (let i = 0; i < input.length; i++) {
        const isUpperCaseOrWhatever = input[i] === input[i].toUpperCase();
        let strLowerCase = input[i].toLowerCase();
        if (strLowerCase === " " && spaceReplacement) {
            newStr += spaceReplacement;
            continue;
        }
        let newLetter: string | undefined = _preset === "uk" && strLowerCase === "г" && i > 0 && input[i - 1].toLowerCase() === "з"
            ? "gh"
            : (i === 0 ? _firstLetterAssociations : _associations)[strLowerCase];
        if ("undefined" === typeof newLetter) {
            newStr += isUpperCaseOrWhatever ? strLowerCase.toUpperCase() : strLowerCase;
        }
        else {
            newStr += isUpperCaseOrWhatever ? newLetter.toUpperCase() : newLetter;
        }
    }
    return newStr;
}

export function IsoDate(date: Date): string {
    function pad(num: number) {
        if (num < 10) {
            return '0' + num;
        }
        return num;
    }

    return date.getFullYear() +
        '-' + pad(date.getMonth() + 1) +
        '-' + pad(date.getDate());
}

export function BuildDate(dateIso: string, timeIso: string): Date {
    const date = dateIso.split('-');
    const time = timeIso.split(':')
    return new Date(parseInt(date[0]), parseInt(date[1]) - 1, parseInt(date[2]), parseInt(time[0]), parseInt(time[1]));
}