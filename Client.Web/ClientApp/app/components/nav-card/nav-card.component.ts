﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { BasketService } from '../shared/services/basket.service';

@Component({
    selector: 'nav-card',
    templateUrl: './nav-card.component.html',
    styleUrls: ['./nav-card.component.scss']
})
export class NavCardComponent {
    constructor(public basketService: BasketService,
        private router: Router) { }

    navigateCart() {
        this.router.navigate(['/cart']);
    }
}
