import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';

import { StorageService, InmemoryStorageService } from './components/shared/services/storage.service';

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        ServerModule,
        AppModuleShared
    ],
    providers: [
        { provide: StorageService, useClass: InmemoryStorageService }
    ]
})
export class AppModule {
}
